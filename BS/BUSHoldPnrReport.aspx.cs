﻿
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

public partial class BS_BUSHoldPnrReport : System.Web.UI.Page
{

    private SqlTransaction ST = new SqlTransaction();
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private clsInsertSelectedFlight CllInsSelectFlt = new clsInsertSelectedFlight();
    DataSet AgencyDDLDS = new DataSet();
    DataSet grdds = new DataSet();
    DataSet fltds = new DataSet();
    private Status sttusobj = new Status();
    SqlConnection con = new SqlConnection();
    ClsCorporate clsCorp = new ClsCorporate();
    protected void Page_Load(object sender, System.EventArgs e)
    {
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href='#' data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Bus </a><a class='current' href='#'>Bus Hold Report</a>";
        try
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (string.IsNullOrEmpty(Session["UID"].ToString()) | Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            if (!IsPostBack)
            {
                CheckEmptyValue();
                DataTable dtExecutive = new DataTable();
                DataTable dtStatus = new DataTable();
                DataSet dsddls = new DataSet();
                dsddls = GetAgentid();
                //dtStatus = dsddls.Tables[0];
                dtExecutive = dsddls.Tables[1];
                ddl_ExecID.AppendDataBoundItems = true;
                ddl_ExecID.Items.Clear();
                ddl_ExecID.Items.Insert(0, "--Select--");
                ddl_ExecID.DataSource = dtExecutive;
                ddl_ExecID.DataTextField = "ExecutiveID";
                ddl_ExecID.DataValueField = "ExecutiveID";
                ddl_ExecID.DataBind();
            }
        }

        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public void CheckEmptyValue()
    {
        try
        {
            string FromDate = null;
            string ToDate = null;
            int lenth = 0;
            // string PgStatus = drpPaymentStatus.Visible == true ? drpPaymentStatus.SelectedValue.ToLower() != "select" ? drpPaymentStatus.SelectedValue : null : null;

            if (String.IsNullOrEmpty(Request["From"]))
            {
                FromDate = "";
            }
            else
            {

                FromDate = Request["From"].Substring(3, 2) + "/" + Request["From"].Substring(0, 2) + "/" + Request["From"].Substring(6, 4);
                FromDate = FromDate + " " + "12:00:00 AM";
            }
            if (String.IsNullOrEmpty(Request["To"]))
            {
                ToDate = "";
            }
            else
            {

                ToDate = Request["To"].Substring(3, 2) + "/" + Request["To"].Substring(0, 2) + "/" + Request["To"].Substring(6, 4);
                ToDate = ToDate + " " + "11:59:59 PM";
            }


            string AgentID = String.IsNullOrEmpty(Request["hidtxtAgencyName"]) | Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"];
            string OrderID = String.IsNullOrEmpty(txtOrderID.Text) ? "" : txtOrderID.Text.Trim();
            string Source = String.IsNullOrEmpty(TxtSource.Text) ? "" : TxtSource.Text.Trim();
            string Destination = String.IsNullOrEmpty(TxtDestination.Text) ? "" : TxtDestination.Text.Trim();
            string orderID = String.IsNullOrEmpty(txtOrderID.Text) ? "" : txtOrderID.Text.Trim();
            string BusOperator = String.IsNullOrEmpty(txtBusOperator.Text) ? "" : txtBusOperator.Text.Trim();
            string TicketNo = String.IsNullOrEmpty(txtTicketNo.Text) ? "" : txtTicketNo.Text.Trim();
            string Pnr = String.IsNullOrEmpty(txtPnr.Text) ? "" : txtPnr.Text.Trim();
            string execID = ddl_ExecID.SelectedValue.ToString(); 
            if (AgentID != "")
            {
                //string str = AgentID;
                //int pos1 = str.IndexOf("(");
                //int pos2 = str.IndexOf(")");
                //lenth = pos2 - pos1;
                //string AgentID1 = str.Substring(pos1 + 1, lenth - 1);
                grdds.Clear();
                grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID, "", Source, Destination, BusOperator, TicketNo, Pnr, execID);
                ViewState["grdds"] = grdds;
                GrdBusReport.DataSource = grdds;
                GrdBusReport.DataBind();
            }
            else
            {
                grdds.Clear();
                grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID, "", Source, Destination, BusOperator, TicketNo, Pnr, execID);
                ViewState["grdds"] = grdds;
                GrdBusReport.DataSource = grdds;
                GrdBusReport.DataBind();

            }


        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    
    public DataSet BUSDetails(string loginid, string usertype, string fromdate, string todate, string orderid, string agentid, string paymentStatus, string Source, string Destination, string BusOperator, string TicketNo, string Pnr ,string execid)
    {
        
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                if(execid=="--Select--")
                {
                    execid="";
                }

                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "SP_BUSHoldReport";
                sqlcmd.Parameters.AddWithValue("@usertype", usertype);
                sqlcmd.Parameters.AddWithValue("@LoginID", loginid);
                sqlcmd.Parameters.AddWithValue("@FormDate", fromdate);
                sqlcmd.Parameters.AddWithValue("@ToDate", todate);
                sqlcmd.Parameters.AddWithValue("@OrderID", orderid);
                sqlcmd.Parameters.AddWithValue("@AgentId", agentid);
                sqlcmd.Parameters.AddWithValue("@PaymentStatus", paymentStatus);
                sqlcmd.Parameters.AddWithValue("@source", Source);
                sqlcmd.Parameters.AddWithValue("@destination", Destination);
                sqlcmd.Parameters.AddWithValue("@BUSOPERATOR", BusOperator);
                sqlcmd.Parameters.AddWithValue("@TICKETNO", TicketNo);
                sqlcmd.Parameters.AddWithValue("@PNR", Pnr);
                sqlcmd.Parameters.AddWithValue("@execid", execid);  
                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                da.Fill(DS);
                con.Close();
                DS.Dispose();
                con.Close();
            }
        }
        catch (Exception ex)
        {
        }
        return DS;
    }
    protected void btn_export_Click(object sender, System.EventArgs e)
    {
        try
        {
            string FromDate = null;
            string ToDate = null;
            int lenth = 0;
            // string PgStatus = drpPaymentStatus.Visible == true ? drpPaymentStatus.SelectedValue.ToLower() != "select" ? drpPaymentStatus.SelectedValue : null : null;
            if (String.IsNullOrEmpty(Request["From"]))
            {
                FromDate = "";
            }
            else
            {
                FromDate = Request["From"].Substring(3, 2) + "/" + Request["From"].Substring(0, 2) + "/" + Request["From"].Substring(6, 4);
                FromDate = FromDate + " " + "12:00:00 AM";
            }
            if (String.IsNullOrEmpty(Request["To"]))
            {
                ToDate = "";
            }
            else
            {
                ToDate = Request["To"].Substring(3, 2) + "/" + Request["To"].Substring(0, 2) + "/" + Request["To"].Substring(6, 4);
                ToDate = ToDate + " " + "11:59:59 PM";
            }
            string AgentID = String.IsNullOrEmpty(Request["hidtxtAgencyName"]) | Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"];
            string OrderID = String.IsNullOrEmpty(txtOrderID.Text) ? "" : txtOrderID.Text.Trim();
            string Source = String.IsNullOrEmpty(TxtSource.Text) ? "" : TxtSource.Text.Trim();
            string Destination = String.IsNullOrEmpty(TxtDestination.Text) ? "" : TxtDestination.Text.Trim();
            string orderID = String.IsNullOrEmpty(txtOrderID.Text) ? "" : txtOrderID.Text.Trim();
            string BusOperator = String.IsNullOrEmpty(txtBusOperator.Text) ? "" : txtBusOperator.Text.Trim();
            string TicketNo = String.IsNullOrEmpty(txtTicketNo.Text) ? "" : txtTicketNo.Text.Trim();
            string Pnr = String.IsNullOrEmpty(txtPnr.Text) ? "" : txtPnr.Text.Trim();
            string execID = ddl_ExecID.SelectedValue.ToString(); 
            if (AgentID != "")
            {
                //string str = AgentID;
                //int pos1 = str.IndexOf("(");
                //int pos2 = str.IndexOf(")");
                //lenth = pos2 - pos1;
                //string AgentID1 = str.Substring(pos1 + 1, lenth - 1);
                grdds.Clear();
                grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID, "", Source, Destination, BusOperator, TicketNo, Pnr, execID);
                STDom.ExportData(grdds);
            }
            else
            {
                grdds.Clear();
                grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID, "", Source, Destination, BusOperator, TicketNo, Pnr, execID);            
                STDom.ExportData(grdds);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }


    public DataSet GetAgentid()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "GetStatusExecutiveID";
                sqlcmd.Parameters.Add("@Module", "Hold");
               
                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                da.Fill(DS);
                con.Close();
                DS.Dispose();
                con.Close();
            }
        }
        catch (Exception ex)
        {
        }
        return DS;
    }

    protected void btn_result_Click(object sender, EventArgs e)
    {
        CheckEmptyValue();
    }
}