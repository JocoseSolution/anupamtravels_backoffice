﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="BusCommsion.aspx.cs" Inherits="BS_BusCommsion" %>

<%@ Register Src="~/UserControl/AccountsControl.ascx" TagPrefix="uc1" TagName="Account" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
   
    <script type="text/javascript">
        function phone_vali() {
            if ((event.keyCode > 47 && event.keyCode < 58) || (event.keyCode == 32) || (event.keyCode == 45))
                event.returnValue = true;
            else
                event.returnValue = false;
        }
    </script>
    <div class="row">
         <div class="container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Agent Type</label>
                                    <asp:DropDownList ID="DropDownListType" runat="server" CssClass="input-text full-width"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Commission Type </label>
                                    <asp:DropDownList ID="ddl_commisionType" runat="server" SelectedValue='<%# Eval("MarkupType")%>' CssClass="input-text full-width">
                                        <asp:ListItem Value="F" Selected="true">Fixed</asp:ListItem>
                                        <asp:ListItem Value="P">Percentage</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Commission</label>
                                    <asp:TextBox runat="server" ID="txtCommision" onkeypress="phone_vali();" MaxLength="6" CssClass="input-text full-width"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status</label>
                                    <asp:DropDownList ID="ddlstatus" runat="server" SelectedValue='<%# Eval("MarkupType")%>' CssClass="input-text full-width">
                                        <asp:ListItem Value="Active">Active</asp:ListItem>
                                        <asp:ListItem Value="InActive">InActive</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Provider Name</label>
                                    <asp:DropDownList ID="ddlprovidername" runat="server" SelectedValue='<%# Eval("MarkupType")%>' CssClass="input-text full-width">
                                        <asp:ListItem Value="RB" Selected="true">RedBus</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>


                   
                        </div>
                        <div class="row">
                                     <div class="col-md-4">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="Button1" runat="server" OnClick="btnAdd_Click" CssClass="btn btn-lg btn-primary" Text="Add New" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <br />
                                    <asp:Label ID="lbl" runat="server" Style="color: #CC0000;"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="row" id="divReport" style="background-color: #fff; overflow: auto; max-height: 500px;" runat="server">
                            <div class="col-md-12">

                                <table style="width: 100%;" cellspacing="10">
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="UP" runat="server">
                                                <ContentTemplate>
                                                    <div class="clear1"></div>
                                                    <div class="col-sm-12">
                                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"
                                                            OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting"
                                                            OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" PageSize="8"
                                                            CssClass="table " Width="100%">
                                                            <Columns>
                                                                <asp:CommandField ShowEditButton="True" />

                                                                <asp:TemplateField HeaderText="Sr.No">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="AG_TYPE" HeaderText="Agent Type" ControlStyle-CssClass="textboxflight1" ReadOnly="true" />
                                                                <asp:TemplateField HeaderText="Commission">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_COMMISSION" runat="server" Text='<%# Eval("COMMISSION")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txt_COMMISSION" runat="server" Text='<%# Eval("COMMISSION")%>' onkeypress="phone_vali();" MaxLength="6"></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Commission_type" HeaderText="Commission_Type" ControlStyle-CssClass="textboxflight1"
                                                                    ReadOnly="true" />

                                                                <asp:TemplateField HeaderText="Agent Status">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblstatus" runat="server" Text='<%# GetStatusVal( Eval("AG_STATUS")) %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate>
                                                                        <asp:DropDownList ID="ddlstatus" runat="server" Width="200px" SelectedValue='<%# Eval("AG_STATUS").ToString() %>'>
                                                                            <asp:ListItem Value="Active" Text="Active"></asp:ListItem>
                                                                            <asp:ListItem Value="Inactive" Text="Inactive"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </EditItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="CREATEDDATE" HeaderText="Created Date" ControlStyle-CssClass="textboxflight1"
                                                                    ReadOnly="true" />
                                                                <asp:CommandField ShowDeleteButton="True" />
                                                            </Columns>
                                                            <RowStyle CssClass="RowStyle" />
                                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                            <PagerStyle CssClass="PagerStyle" />
                                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                            <HeaderStyle CssClass="HeaderStyle" />
                                                            <EditRowStyle CssClass="EditRowStyle" />
                                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                                        </asp:GridView>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
                                                <ProgressTemplate>
                                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                                    </div>
                                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                                        Please Wait....<br />
                                                        <br />
                                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                                        <br />
                                                    </div>
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

