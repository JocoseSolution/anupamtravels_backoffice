﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
public partial class BS_HoldBusUpdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {              
                TicketDetail();              
                BindTravellerInformation();
            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);

            }
        }

    }
    private void TicketDetail()
    {
        try
        {
            string OrderId = Request.QueryString["OrderId"];
           // lbl_orderid.Text = OrderId.ToString();

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }
protected void btn_update_Click(object sender, System.EventArgs e)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
        string OrderId = Request.QueryString["OrderId"];
        string ticketno = Txt_tktno.Text.Trim().ToString();
        string PNR = txt_PNR.Text.Trim().ToString();
        //string optcontact = optcontactid.Text.Trim().ToString();
        //string canpolicy = canpolicyid.Text.Trim().ToString();
       
        try
        {
            SqlCommand cmd = new SqlCommand("USP_updatePnrBUSSp_PP");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Order_id", OrderId);
            cmd.Parameters.AddWithValue("@TicketNo", ticketno);
            cmd.Parameters.AddWithValue("@execid", Session["UID"].ToString());
            cmd.Parameters.AddWithValue("@pnr", PNR);
            //cmd.Parameters.AddWithValue("@optcontact", optcontact);
            //cmd.Parameters.AddWithValue("@pnr", canpolicy);
            cmd.Connection = con;
            con.Open();
            cmd.ExecuteNonQuery().ToString();
          
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {

            con.Close();
        }

        ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Ticket Updated Successfully.');javascript: window.close();window.opener.location=window.opener.location.href;", true);

    }

    



public void BindTravellerInformation()
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);	
	try {
        
		string OrderId = Request.QueryString["OrderId"];      
		SqlDataAdapter adap = new SqlDataAdapter("usp_BUSPaxDetails_lookup", con);
		adap.SelectCommand.CommandType = CommandType.StoredProcedure;
		adap.SelectCommand.Parameters.AddWithValue("@orderid", OrderId.Trim());
		DataTable dt2 = new DataTable();
		adap.Fill(dt2);
        tdRefNo.InnerText = dt2.Rows[0]["BOOKING_REF"].ToString();
		GvTravellerInformation.DataSource = dt2;
		GvTravellerInformation.DataBind();
	}
    catch (Exception ex)
    {
        clsErrorLog.LogInfo(ex);

    }
    finally
    {
        con.Close();
    }

}



}