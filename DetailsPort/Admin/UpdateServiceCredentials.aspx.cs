﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DetailsPort_Admin_UpdateServiceCredentials : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            if (string.IsNullOrEmpty(Convert.ToString(Session["User_Type"])))
            {
                Response.Redirect("~/Login.aspx");
            }

            string Id = Request.QueryString["id"];
            HdnId.Value = Id;
            if (!string.IsNullOrEmpty(Convert.ToString(Id)))
            {
                if (!IsPostBack)
                {
                    BindCredential(Id);
                }
            }
            else
            {
                Response.Redirect("ServiceCredentials.aspx", false);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }


    private void BindCredential(string Id)
    {
        DataTable dt = new DataTable();
        try
        { 
        if (!string.IsNullOrEmpty(Convert.ToString(Id)))
        {
            dt = GetCredential(Convert.ToInt32(Id));
            if (dt.Rows.Count > 0)
            {
                //string CorporateID = Convert.ToString(TxtCorporateID.Text);
                //string UserID = Convert.ToString(TxtUserID.Text);
                //string Password = Convert.ToString(TxtPassword.Text);
                //string LoginID = Convert.ToString(TxtLoginID.Text);
                //string LoginPwd = Convert.ToString(TxtLoginPwd.Text);
                //string ServerUrlOrIP = Convert.ToString(TxtServerUrlOrIP.Text);
                //string BkgServerUrlOrIP = Convert.ToString(TxtBkgServerUrlOrIP.Text);
                //string Port = Convert.ToString(TxtPort.Text);
                //string Provider = Convert.ToString(DdlProvider.Text);
                //string CarrierAcc = Convert.ToString(TxtCarrierAcc.Text);
                //string SearchType = Convert.ToString(TxtSearchType.Text);
                //string Status = Convert.ToString(DdlStatus.Text);
                //string ResultFrom = Convert.ToString(TxtResultFrom.Text);
                //string CrdType = Convert.ToString(TxtCrdType.Text);
                //int flag = InsertServiceCredential(CorporateID, UserID, Password, LoginID, LoginPwd, ServerUrlOrIP, BkgServerUrlOrIP, Port, Provider, CarrierAcc, SearchType, Status, ResultFrom, CrdType);

                TxtAirline.Text = Convert.ToString(dt.Rows[0]["AirlineName"]);
                txtTripType.Text = Convert.ToString(dt.Rows[0]["TripTypeName"]);

                TxtCorporateID.Text = Convert.ToString(dt.Rows[0]["CorporateID"]);
                TxtUserID.Text = Convert.ToString(dt.Rows[0]["UserID"]);
                TxtPassword.Text = Convert.ToString(dt.Rows[0]["Password"]);
                TxtLoginID.Text = Convert.ToString(dt.Rows[0]["LoginID"]);
                TxtServerUrlOrIP.Text = Convert.ToString(dt.Rows[0]["ServerUrlOrIP"]);
                TxtBkgServerUrlOrIP.Text = Convert.ToString(dt.Rows[0]["BkgServerUrlOrIP"]);
               // TxtPort.Text = Convert.ToString(dt.Rows[0]["Port"]);                
                TxtCarrierAcc.Text = Convert.ToString(dt.Rows[0]["CarrierAcc"]);
               // TxtSearchType.Text = Convert.ToString(dt.Rows[0]["SearchType"]);                
                DdlResultFrom.SelectedValue = Convert.ToString(dt.Rows[0]["ResultFrom"]);
                DdlCrdType.SelectedValue = Convert.ToString(dt.Rows[0]["CrdType"]);
                lblProvider.Text = Convert.ToString(dt.Rows[0]["Provider"]);
                TxtLoginPwd.Text = Convert.ToString(dt.Rows[0]["LoginPwd"]);
                 string Status =  Convert.ToString(dt.Rows[0]["Status"]).ToLower();
                string Provider=Convert.ToString(dt.Rows[0]["Provider"]);
                if(Provider=="1G" ||Provider=="1GINT")
                {
                    CorporateID.Visible=true;
                    CarrierAcc.Visible=true;
                    ResultFrom.Visible=false;
                    Url.Visible=false;
                    LoginID.Visible=false;
                    LoginPassword.Visible=false;
                }
                //else if(Provider=="G8")
                //{
                //    CorporateID.Visible=true;
                //    CarrierAcc.Visible=false;
                //    ResultFrom.Visible=true;
                //    Url.Visible=false;
                //    LoginID.Visible=true;
                //    LoginPassword.Visible=true;
                //}
                else{ 
                    CorporateID.Visible=true;
                    CarrierAcc.Visible=false;
                    ResultFrom.Visible=true;
                    Url.Visible=false;
                    LoginID.Visible = false;
                    LoginPassword.Visible = false;
                } 
                if(Status=="true")
                {
                    DdlStatus.SelectedValue="true";                      
                }
                else if(Status=="false")
                {
                    DdlStatus.SelectedValue="false";
                }                
               // int flag = InsertServiceCredential(CorporateID, UserID, Password, LoginID, LoginPwd, ServerUrlOrIP, BkgServerUrlOrIP, Port, Provider, CarrierAcc, SearchType, Status, ResultFrom, CrdType);               
            }
            else
            {
                BtnSubmit.Visible = false;
       }
        }

        }
        catch (Exception ex)
        {          
            clsErrorLog.LogInfo(ex);
        }
    }

    public DataTable GetCredential(int ID)
    {
        DataTable dt = new DataTable();
        try
        {
            adap = new SqlDataAdapter("SpFlightServiceCredential", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@Counter", ID);
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "GETBYID");
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            adap.Dispose();
        }
        return dt;
    }

    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string CorporateID = Convert.ToString(TxtCorporateID.Text);
            string UserID = Convert.ToString(TxtUserID.Text);
            string Password = Convert.ToString(TxtPassword.Text);
            string LoginID = Convert.ToString(TxtLoginID.Text);
            string LoginPwd = Convert.ToString(TxtLoginPwd.Text);
            string ServerUrlOrIP = Convert.ToString(TxtServerUrlOrIP.Text);
            string BkgServerUrlOrIP = Convert.ToString(TxtBkgServerUrlOrIP.Text);
            string Port = "";// Convert.ToString(TxtPort.Text);
            string Provider = Convert.ToString(lblProvider.Text);
            string CarrierAcc = Convert.ToString(TxtCarrierAcc.Text);
            string SearchType = "";//Convert.ToString(TxtSearchType.Text);
            string Status = Convert.ToString(DdlStatus.SelectedValue);           
            string ResultFrom = Convert.ToString(DdlResultFrom.SelectedValue);            
            string CrdType = Convert.ToString(DdlCrdType.SelectedValue);  
            int flag = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(HdnId.Value)))
            {
                int Counter = Convert.ToInt32(HdnId.Value);
                //int flag = InsertServiceCredential(CorporateID, UserID, Password, LoginID, LoginPwd, ServerUrlOrIP, BkgServerUrlOrIP, Port, Provider, CarrierAcc, SearchType, Status, ResultFrom, CrdType);
                flag = UpdateCredential(Counter, CorporateID, UserID, Password, LoginID, LoginPwd, ServerUrlOrIP, BkgServerUrlOrIP, Port, Provider, CarrierAcc, SearchType, Status, ResultFrom, CrdType);
                if (flag > 0)
                {
                    BindCredential(Convert.ToString(HdnId.Value));
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully updated.');", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Record successfully updated.');window.location='ServiceCredentials.aspx'; ", true);
                }
                else
                {
                    BindCredential(Convert.ToString(HdnId.Value));
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record not updated, try again!!');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');window.location='CommissionMaster.aspx'; ", true);
            }


        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='CommissionMaster.aspx'; ", true);
        }

    }

    private int UpdateCredential(int Counter, string CorporateID, string UserID, string Password, string LoginID, string LoginPwd, string ServerUrlOrIP, string BkgServerUrlOrIP, string Port, string Provider, string CarrierAcc, string SearchType, string Status, string ResultFrom, string CrdType)
    {

        int flag = 0;
        string CreatedBy = Convert.ToString(Session["UID"]);
        string ActionType = "update";
        try
        {
            SqlCommand cmd = new SqlCommand("SpFlightServiceCredential", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Counter", Counter);
            cmd.Parameters.AddWithValue("@CorporateID", CorporateID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@Password", Password);
            cmd.Parameters.AddWithValue("@LoginID", LoginID);
            cmd.Parameters.AddWithValue("@LoginPwd", LoginPwd);
            cmd.Parameters.AddWithValue("@ServerUrlOrIP", ServerUrlOrIP);
            cmd.Parameters.AddWithValue("@BkgServerUrlOrIP", BkgServerUrlOrIP);
            cmd.Parameters.AddWithValue("@Port", Port);
            cmd.Parameters.AddWithValue("@Provider", Provider);
            cmd.Parameters.AddWithValue("@CarrierAcc", CarrierAcc);
            cmd.Parameters.AddWithValue("@SearchType", SearchType);
            cmd.Parameters.AddWithValue("@Status", Convert.ToBoolean(Status));
            cmd.Parameters.AddWithValue("@ResultFrom", ResultFrom);
            cmd.Parameters.AddWithValue("@CrdType", CrdType);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ActionType", ActionType);
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            con.Close();
        }
        return flag;

    }


}