﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="IntlDiscountMaster.aspx.vb" Inherits="DetailsPort_Admin_IntlDiscountMaster" %>

<%@ Register Src="~/UserControl/Settings.ascx" TagPrefix="uc1" TagName="Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>

    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>

    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />




    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>

  

    <div class="row">
       <div class="col-md-12"  >
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight Setting > Intl. Discount Master</h3>
                    </div>
                    <div class="panel-body">


                        <div class="row">

                            <%--<div class="col-md-4">
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="Select Type" Font-Bold="True"></asp:Label>
                                    <asp:DropDownList ID="ddl_type" runat="server" OnSelectedIndexChanged="ddl_type_SelectedIndexChanged"
                                        CssClass="form-control" AutoPostBack="True">
                                        <asp:ListItem Value="Select Type" Selected="True" Text="--Select Type--"></asp:ListItem>
                                        <asp:ListItem Text="All Type" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>--%>
                             
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">GroupType :   </label>
                                    &nbsp;&nbsp;<input type="checkbox" id="CheckBoxAllType" name="All Type" value="All Type" onclick="javascript: allType();" class="checked" />
                                    All Group Type
                                   <input type="hidden" id="HiddenAlltype" runat="server" value="" />
                                    <asp:DropDownList ID="ddl_ptype" CssClass="form-control" runat="server" AppendDataBoundItems="true" >                                                                            
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Airline :</label>
                                    <asp:DropDownList ID="ddl_Pairline" CssClass="form-control" runat="server" data-placeholder="Choose a Airline..."
                                        TabIndex="2">
                                    </asp:DropDownList>
                                </div>                       
                        </div>


                         <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Commission Basic :</label>
                                    <asp:TextBox ID="txt_basic" CssClass="form-control" runat="server" onKeyPress="return isNumberKey(event)"
                                        MaxLength="5" Text="0"></asp:TextBox>
                                </div>

                            </div>
                        

                             
                            <div class="col-md-3" id="tr_PLB" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Commission YQ:</label>
                                    <asp:TextBox ID="txt_CYQ" CssClass="form-control" runat="server" onKeyPress="return isNumberKey(event)" MaxLength="5"
                                        Text="0"></asp:TextBox>
                                </div>
                            </div>
                              </div>

                        <div class="row">
                          

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Commission(BASIC+YQ) :</label>
                                    <asp:TextBox ID="txt_CYB" runat="server" CssClass="form-control" onKeyPress="return isNumberKey(event)" MaxLength="5"
                                        Text="0"></asp:TextBox>
                                </div>
                            </div>

                                             
                 
                               <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Class</label><br />
                                    <asp:DropDownList ID="ddl_PLBClass" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="All">--All--</asp:ListItem>
                            <asp:ListItem Value="C">Business</asp:ListItem>
                            <asp:ListItem Value="Y">Economy</asp:ListItem>
                            <asp:ListItem Value="F">First</asp:ListItem>
                            <asp:ListItem Value="W">Premium Economy</asp:ListItem>
                            <asp:ListItem Value="P">Premium First</asp:ListItem>

                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">RBD : &nbsp; Ex =&gt; A,B,C etc </label>
                                    <asp:TextBox ID="txt_PRBD" runat="server" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                   <%-- <asp:RegularExpressionValidator ID="RegExp1" runat="server" ErrorMessage="characters Formate not matched" ControlToValidate="txt_PRBD" ValidationExpression="^[A-Z]$" />--%>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Sector :Ex=&gt; DLI-AMH   </label>
                                    <asp:TextBox ID="txt_Sector" runat="server" CssClass="form-control" MaxLength="7"></asp:TextBox>&nbsp;  
                                <asp:RegularExpressionValidator ID="Regex2" runat="server" ControlToValidate="txt_Sector" ErrorMessage="characters Formate not matched" ValidationExpression="^[A-Z]{3}-[A-Z]{3}$" />
                                </div>
                            </div>

                            </div>
                         <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">PLB Basic:</label>
                                    <asp:TextBox ID="txt_Pbasic" CssClass="form-control" runat="server" onKeyPress="return isNumberKey(event)"
                                        MaxLength="5" Text="0"></asp:TextBox>
                                </div>
                            </div>
                     
                    
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">PLB (BASIC+YQ) :</label>
                                    <asp:TextBox ID="txt_Pyqb" CssClass="form-control" runat="server" onKeyPress="return isNumberKey(event)"
                                        MaxLength="5" Text="0"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Start Date:</label>
                                    <%--<asp:TextBox ID="From" CssClass="form-control" runat="server" name="From" ReadOnly="true"></asp:TextBox>--%>
                                    <input type="text" name="From" id="From" class="form-control" readonly="readonly" />
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">End Date:</label>
                                    <input type="text" name="To" id="To" class="form-control" readonly="readonly" />

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Class</label><br />
                                    <asp:DropDownList ID="ddl_COMMClass" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="All">--All--</asp:ListItem>
                                         <asp:ListItem Value="C">Business</asp:ListItem>
                                          <asp:ListItem Value="Y">Economy</asp:ListItem>
                                            <asp:ListItem Value="F">First</asp:ListItem>
                                           <asp:ListItem Value="W">Premium Economy</asp:ListItem>
                                           <asp:ListItem Value="P">Premium First</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Remark</label>
                                    <asp:TextBox ID="txt_Remark" CssClass="form-control" runat="server" MaxLength="5" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <br />
                                <div class="form-group">
                                    <asp:Button ID="btn_submit" Text="Submit" CssClass="button buttonBlue" runat="server" OnClientClick="return PLBValidation()"
                                        OnClick="btn_submit_Click" />
                                </div>
                            </div> 
                              <div class="col-md-3">
                                <br />
                                <div class="form-group">
                                    <asp:Button ID="btn_result" Text="Search" CssClass="button buttonBlue" runat="server"  />
                                </div>
                            </div>                                    
                        </div>

                         <div class="row">

                                 <div class="col-md-3">
                                          <div class="form-group">
                                    <asp:Button ID="btn_export" runat="server" CssClass="button buttonBlue" Text="Export" />
                                </div>
                            </div>
                          <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Style="color: #CC0000;" Font-Bold="True"
                                        Visible="False"></asp:Label>
                                </div>
                            </div>
                             </div>

                        <div class="row">
                            <div class="col-md-12">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow:auto; max-height: 500px;">
                                    <ContentTemplate>
                                        <asp:GridView ID="grd_P_IntlDiscount" runat="server" AutoGenerateColumns="false"
                                            CssClass="table"
                                            GridLines="None" Width="100%" PageSize="30" OnRowCancelingEdit="grd_P_IntlDiscount_RowCancelingEdit"
                                            OnRowDeleting="grd_P_IntlDiscount_RowDeleting" OnRowEditing="grd_P_IntlDiscount_RowEditing"
                                            OnRowUpdating="grd_P_IntlDiscount_RowUpdating">
                                            <Columns>
                                                <asp:TemplateField HeaderText="GROUP TYPE">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_group" runat="server" Text='<%#Eval("GroupType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AIRLINE NAME">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_airline" runat="server" Text='<%#Eval("AirlineName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AIRLINE CODE">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_airlinecode" runat="server" Text='<%#Eval("AirlineCode") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="RBD">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_rbd" runat="server" Text='<%#Eval("RBD") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txt_RBD" runat="server" Text='<%#Eval("RBD") %>' MaxLength="30" Width="80px" BackColor="#ffff66"></asp:TextBox>
                                                        <%-- <asp:RequiredFieldValidator ID="txt_RBD1" ControlToValidate="txt_RBD" runat="server"
                                                        ErrorMessage="Required" ValidationGroup="upd" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                                      <%--  <asp:RegularExpressionValidator ID="RegExp1" runat="server" ErrorMessage="characters Formate not matched" ControlToValidate="txt_RBD" ValidationExpression="^[A-Z]{1,10}$" />--%>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ORIGIN">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Sector" runat="server" Text='<%#Eval("Sector") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txt_Sector" runat="server" Text='<%#Eval("Sector") %>' Width="80px" MaxLength="200"
                                                            BackColor="#ffff66"></asp:TextBox>
                                                          <%--<asp:RequiredFieldValidator ID="txt_Sector1" ControlToValidate="txt_Sector" runat="server"
                                                        ErrorMessage="Required" ValidationGroup="upd" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                                       <%-- <asp:RegularExpressionValidator ID="Regex3" runat="server" ControlToValidate="txt_Sector" ErrorMessage="characters Formate not matched" ValidationExpression="^[A-Z]{3}-[A-Z]{3}$" />--%>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PLB BASIC">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Pbasic" runat="server" Text='<%#Eval("PlbOnBasic") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txt_PBasic" runat="server" Text='<%#Eval("PlbOnBasic") %>' onKeyPress="return isNumberKey(event)"
                                                            MaxLength="10" Width="60px" BackColor="#ffff66"></asp:TextBox>
                                                          <asp:RequiredFieldValidator ID="txt_PBasic1" ControlToValidate="txt_PBasic" runat="server"
                                                        ErrorMessage="Required" ValidationGroup="upd" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PLB ON (BASIC+YQ)">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Pbasicyq" runat="server" Text='<%#Eval("PlbOnBasicYq") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txt_PBasicYQ" runat="server" Text='<%#Eval("PlbOnBasicYq") %>' MaxLength="5" onKeyPress="return isNumberKey(event)"
                                                            Width="60px" BackColor="#ffff66"></asp:TextBox>
                                                          <asp:RequiredFieldValidator ID="txt_PBasicYQ1" ControlToValidate="txt_PBasicYQ" runat="server"
                                                        ErrorMessage="Required" ValidationGroup="upd" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="START DATE">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_StartDate" runat="server" Text='<%#Eval("StartDate") %>'></asp:Label>

                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                    
                                                        <asp:TextBox ID="txt_StartDate" runat="server" Text='<%#Bind("StartDate") %>' Width="70px" MaxLength="14"
                                                            CssClass="date" BackColor="#ffff66"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="txt_StartDate1" ControlToValidate="txt_StartDate" runat="server"
                                                        ErrorMessage="Required" ValidationGroup="upd" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="regexpName" runat="server"
                                                            ErrorMessage="Please enter validate Date"
                                                            ControlToValidate="txt_StartDate"
                                                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$" />
                                                      
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="END DATE">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_EndDate" runat="server" Text='<%#Eval("EndDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txt_EndDate" runat="server" Text='<%#Bind("EndDate") %>' Width="70px" MaxLength="14"
                                                            CssClass="date" BackColor="#ffff66"></asp:TextBox>
                                                         <asp:RequiredFieldValidator ID="txt_EndDate1" ControlToValidate="txt_EndDate" runat="server"
                                                        ErrorMessage="Required" ValidationGroup="upd" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="regexpName1" runat="server"
                                                            ErrorMessage="Please enter validate Date."
                                                            ControlToValidate="txt_EndDate"
                                                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$" />
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="REMARK">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Remark" runat="server" Text='<%#Eval("Remark") %>' Style="text-wrap: inherit;"></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txt_Remark" runat="server" Text='<%#Bind("Remark") %>' Width="150px" MaxLength="200"
                                                            BackColor="#ffff66" TextMode="MultiLine"></asp:TextBox>
                                                         <%--<asp:RequiredFieldValidator ID="txt_Remark1" ControlToValidate="txt_Remark" runat="server" ErrorMessage="Required" ValidationGroup="upd" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="COMMISSION ON BASIC">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Cbasic" runat="server" Text='<%#Eval("CommisionOnBasic") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txt_CBasic" runat="server" Text='<%#Eval("CommisionOnBasic") %>'
                                                            onKeyPress="return isNumberKey(event)" Width="60px" BackColor="#ffff66"></asp:TextBox>
                                                         <asp:RequiredFieldValidator ID="txt_CBasic1" ControlToValidate="txt_CBasic" runat="server" ErrorMessage="Required" ValidationGroup="upd" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="COMMISSION ON YQ">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Cyq" runat="server" Text='<%#Eval("CommissionOnYq") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txt_CYQ" runat="server" Text='<%#Eval("CommissionOnYq") %>' Width="60px"
                                                            onKeyPress="return isNumberKey(event)" BackColor="#ffff66"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="txt_CYQ1" ControlToValidate="txt_CYQ" runat="server" ErrorMessage="Required" ValidationGroup="upd" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="COMMISSION ON (BASIC+YQ)">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_Cbasicyq" runat="server" Text='<%#Eval("CommisionOnBasicYQ") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txt_CBasicYQ" runat="server" Text='<%#Eval("CommisionOnBasicYQ") %>'
                                                            onKeyPress="return isNumberKey(event)" Width="60px" BackColor="#ffff66"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="txt_CBasicYQ1" ControlToValidate="txt_CBasicYQ" runat="server" ValidationGroup="upd" ErrorMessage="Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PLB Class">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_plbClass" runat="server" Text='<%#Eval("PLBClass")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="ddl_PLBClass" runat="server" Width="150px" DataTextField='<%#Eval("PLBClass")%>'>
                                                            <asp:ListItem Value="All">--All--</asp:ListItem>
                                                            <asp:ListItem>Business</asp:ListItem>
                                                            <asp:ListItem>Economy</asp:ListItem>
                                                            <asp:ListItem>First</asp:ListItem>
                                                            <asp:ListItem>Premium Economy</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Comm Class">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_commclass" runat="server" Text='<%#Eval("CommClass")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="ddl_CommClass" runat="server" Width="150px" DataTextField='<%#Eval("CommClass")%>'>
                                                            <asp:ListItem Value="All">--All--</asp:ListItem>
                                                            <asp:ListItem>Business</asp:ListItem>
                                                            <asp:ListItem>Economy</asp:ListItem>
                                                            <asp:ListItem>First</asp:ListItem>
                                                            <asp:ListItem>Premium Economy</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="EDIT">
                                                    <ItemTemplate>
                                                        <asp:Button ID="lnledit" runat="server" Text="Edit" CommandName="Edit" Font-Bold="true"
                                                            CssClass="newbutton_2" />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Button ID="lnlupdate" runat="server" Text="Update" CommandName="Update" Font-Bold="true" ValidationGroup="upd"
                                                            CssClass="newbutton_2" />
                                                        <asp:Button ID="lnlcancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true"
                                                            CssClass="newbutton_2" />
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btn_delete" CssClass="newbutton_2" runat="server" Text="Delete" CommandName="Delete"
                                                            OnClientClick="return ConfirmDelete()" Font-Bold="true" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
      <%--<script type="text/javascript">
          $(document.ready(function () {
              $(".date").datepicker({
                  dateFormat: 'dd/mm/yy',
                  minDate: 0,
              });
          }));
    </script>--%>

    <script type="text/javascript">
        function pageLoad() {
            $('.date').unbind();
            $('.date').datepicker(

                 {
                     numberOfMonths: 1,

                     autoSize: true, dateFormat: 'dd-mm-yy', closeText: 'X', duration: 'slow', gotoCurrent: true, changeMonth: false,
                     changeYear: false, hideIfNoPrevNext: false, maxDate: '1y', minDate: 0, navigationAsDateFormat: true, defaultDate: +0, showAnim: 'toggle', showOtherMonths: true,
                     selectOtherMonths: true, showoff: "button", buttonImageOnly: true, onSelect: UpdateToDate
                 }

                );
        }


</script>

   <%-- <script type="text/javascript">
        $(document).ready(function(){
            $("#txt_StartDate").datepicker();
        });


         </script>--%>

    <script type="text/javascript">

        $(function () {
            $("#From").datepicker(
                   {
                       numberOfMonths: 1,

                       autoSize: true, dateFormat: 'dd-mm-yy', closeText: 'X', duration: 'slow', gotoCurrent: true, changeMonth: false,
                       changeYear: false, hideIfNoPrevNext: false, maxDate: '1y', minDate: 0, navigationAsDateFormat: true, defaultDate: +0, showAnim: 'toggle', showOtherMonths: true,
                       selectOtherMonths: true, showoff: "button", buttonImageOnly: true, onSelect: UpdateToDate
                   }
              )

        });
        $(function () {
            $("#To").datepicker(
                   {
                       numberOfMonths: 1,

                       autoSize: true, dateFormat: 'dd-mm-yy', closeText: 'X', duration: 'slow', gotoCurrent: true, changeMonth: false,
                       changeYear: false, hideIfNoPrevNext: false, maxDate: '1y', minDate: 0, navigationAsDateFormat: true, defaultDate: +0, showAnim: 'toggle', showOtherMonths: true,
                       selectOtherMonths: true, showoff: "button", buttonImageOnly: true, onSelect: UpdateToDate
                   }
              )

        });
        function UpdateToDate(dateText, inst) {
            $("From").datepicker("option", { maxDate: dateText });
            $("To").datepicker("option", { minDate: dateText });
        }


     

    </script>
    <script type="text/javascript">


        function allType() {
            $('#<%=HiddenAlltype.ClientID %>').val("");
            if ($("#CheckBoxAllType").is(":checked")) {

                $('#<%=ddl_ptype.ClientID %>').hide();
                $('#<%=HiddenAlltype.ClientID %>').val("0");

            }
            else {
                $('#<%=ddl_ptype.ClientID %>').show();
                $('#<%=HiddenAlltype.ClientID %>').val("");
            }
        }
    </script>

    <script type="text/javascript" language="javascript">
        function ValidateCom() {
            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_Cairline").value == "--Select Airline--") {

                alert('Please Select AirlineName');

                return false;
            }

            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_basic").value == "") {

                alert('Please Fill Basic');
                document.getElementById("ctl00_ContentPlaceHolder1_txt_basic").focus();
                return false;
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_CYQ").value == "") {

                alert('Please Fill CommissionYQ');
                document.getElementById("ctl00_ContentPlaceHolder1_txt_CYQ").focus();
                return false;
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_txt_CYB").value == "") {

                alert('Please Fill Commission On YQ and Basic');
                document.getElementById("ctl00_ContentPlaceHolder1_txt_CYB").focus();
                return false;
            }
            if (confirm("Are you sure!"))
                return true;
            return false;
        }
        //function PLBValidation() {
        //    if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_Pairline").value == "--Select Airline--") {

        //        alert('Please Fill AirlineName');

        //        return false;
        //    }

        //    if (document.getElementById("ctl00_ContentPlaceHolder1_txt_Pbasic").value == "") {

        //        alert('Please Fill PLB Basic');
        //        document.getElementById("ctl00_ContentPlaceHolder1_txt_Pbasic").focus();
        //        return false;
        //    }
        //    if (document.getElementById("ctl00_ContentPlaceHolder1_txt_Pyqb").value == "") {

        //        alert('Please Fill PLB YQ with Basic');
        //        document.getElementById("ctl00_ContentPlaceHolder1_txt_Pyqb").focus();
        //        return false;
        //    }



        //    if (document.getElementById("From").value == "") {

        //        alert('Please Fill Start Date');
        //        document.getElementById("From").focus();
        //        return false;
        //    }

        //    if (document.getElementById("To").value == "") {

        //        alert('Please Fill End Date');
        //        document.getElementById("To").focus();
        //        return false;
        //    }




        //    if (document.getElementById("ctl00_ContentPlaceHolder1_txt_basic").value == "") {

        //        alert('Please Fill Commission Basic');
        //        document.getElementById("ctl00_ContentPlaceHolder1_txt_basic").focus();
        //        return false;
        //    }
        //    if (document.getElementById("ctl00_ContentPlaceHolder1_txt_CYQ").value == "") {

        //        alert('Please Fill CommissionYQ');
        //        document.getElementById("ctl00_ContentPlaceHolder1_txt_CYQ").focus();
        //        return false;
        //    }
        //    if (document.getElementById("ctl00_ContentPlaceHolder1_txt_CYB").value == "") {

        //        alert('Please Fill Commission On YQ and Basic');
        //        document.getElementById("ctl00_ContentPlaceHolder1_txt_CYB").focus();
        //        return false;
        //    }



        //    if (confirm("Are you sure!"))
        //        return true;
        //    return false;
        //}
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 8) || (charCode == 46)) {
                return true;
            }
            else {

                return false;
            }
        }
    </script>

    <script type="text/javascript" language="javascript">
        function ConfirmDelete() {
            if (confirm("Are you sure!"))
                return true;
            return false;
        }
    </script>
</asp:Content>


