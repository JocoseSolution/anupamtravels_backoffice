﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="CItyManagementPanel.aspx.cs" Inherits="DetailsPort_CItyManagementPanel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .frmContainer {
            width: 60%;
            margin: auto;
        }

        .gv {
            width: 60%;
            margin: auto;
            color: #93928d;
        }

            .gv th {
                height: 35px;
                background-color: #7e1818;
                color: #fff;
            }

        .table {
            width: 60%;
            margin: auto;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%= btnSubmit.ClientID %>').click(function () {
                var state = $('#<%= ddlState.ClientID %>').val();
                var city = $('#<%= txtCity.ClientID %>').val();
                if (state == 0) {
                    window.alert("Select state from dropdouwn.!");
                    return false;
                }
                if (city == '') {
                    window.alert("Enter the city name.!");
                    return false;
                }
                return true;
            })
        });
        $(document).ready(function () {
            $('#<%= txtCity.ClientID%>').keypress(function (e) {
                var uniCode = e.charChode ? e.charChode : e.keyCode;
                if ((uniCode == 8) || (uniCode == 9) || (uniCode > 64 && uniCode < 91) || (uniCode > 96 && uniCode < 123)) {
                    return true;
                }
                else {
                    window.alert("This field requires only character");
                    return false;
                }
            })
        });
        $(document).ready(function () {
            $('#<%= btnSearch.ClientID%>').click(function () {
                var state = $('#<%= ddlState.ClientID %>').val();
                var CityValue = $('#<%= txtCity.ClientID%>').val();
                if (state == 0) {
                    window.alert("Select state from dropdouwn.!");
                    return false;
                }
                if (CityValue == '') {
                    window.alert("Enter the city name.!");
                    return false;
                }
                return true;
            })
        });
    </script>
    <div class="container-fluid" style="padding-right: 35px">
        <div class="page-wrapperss">

            <div class="panel panel-primary">

                <div class="panel-body">
                    <%--       <table class="table">
            <tr>
                <td><asp:Label ID="lblState" runat="server" Text="Select State:" AssociatedControlID="ddlState"></asp:Label></td>
                <td><asp:DropDownList ID="ddlState" CssClass="input-text full-width" runat="server" Height="25px" Width="210px" AutoPostBack="True" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>--%>
                    <div class="row">
                        <div class="col-xs-2" style="margin-left: 300px">
                            <div class="input-group">
                                <%--<asp:Label ID="lblState" runat="server" Text="Select State:" AssociatedControlID="ddlState"></asp:Label>--%>
                                <asp:DropDownList ID="ddlState"  runat="server" AutoPostBack="True" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                                </asp:DropDownList>
                                <%--   <asp:RequiredFieldValidator ID="RFVMK" runat="server" ControlToValidate="Roletxt" ErrorMessage="*"
                                    Display="dynamic" ValidationGroup="group2"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>

                        <div class="col-xs-2" style="margin-left: 34px;">
                            <div class="input-group">
                                <%-- <asp:Label ID="lblCity" runat="server" Text="Enter City Name:" AssociatedControlID="txtCity"></asp:Label>--%>
                                <asp:TextBox ID="txtCity" placeholder="Enter City Name" CssClass="form-control input-text full-width" runat="server" class="txtcity"></asp:TextBox>
                                <span class="input-group-addon" style="background-color: #49cced">
                                    <span class="fa fa-building"></span>
                                </span>
                            </div>
                        </div>
                          
                   
                        <div class="col-md-4" style="margin-top: 3px;">
                            <div class="input-group">

                                <asp:Button ID="btnSubmit" CssClass="btn btn-success" runat="server" Text="Add City" OnClick="btnSubmit_Click" />

                                &nbsp

                                <asp:Button ID="btnSearch" CssClass="btn btn-success" runat="server" Text="Search City" OnClick="btnSearch_Click" />
                            </div>
                        </div>
                  

                    
                    </div>


                    <%--            <tr>
                <td><asp:Label ID="lblCity" runat="server" Text="Enter City Name:" AssociatedControlID="txtCity" ></asp:Label></td>
                <td><asp:TextBox ID="txtCity" CssClass="input-text full-width" runat="server" Height="25px" Width="200px" class="txtcity"></asp:TextBox></td>
            </tr>--%>
                    <%--       <tr>
                <td></td>
                <td><asp:Button ID="btnSubmit" CssClass="btn btn-lg btn-primary"  runat="server" Text="Add City" Height="30px" Width="100px" OnClick="btnSubmit_Click" />
                    &nbsp;&nbsp;<asp:Button ID="btnSearch" runat="server" Text="Search City" Height="30px" Width="100px" OnClick="btnSearch_Click" />
                </td>
            </tr>
        </table>--%>
                    <br />
                    <asp:GridView ID="cityGridview" runat="server" CssClass="table table-striped table-bordered table-hover" EmptyDataText="Sorry,no city found..!" AutoGenerateColumns="False" OnRowCancelingEdit="cityGridview_RowCancelingEdit" OnRowEditing="cityGridview_RowEditing" OnRowUpdating="cityGridview_RowUpdating" OnRowDeleting="cityGridview_RowDeleting">
                        <Columns>
                            <asp:TemplateField HeaderText="City Name">
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="counterHiddenField" Value='<%# Eval("COUNTER") %>' />
                                    <asp:Label ID="lblCityName" runat="server" Text='<%# Eval("CITY") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:HiddenField runat="server" ID="counterHiddenField" Value='<%# Eval("COUNTER") %>' />
                                    <asp:TextBox ID="txtEditCity" runat="server" Text='<%# Eval("CITY") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqCityName" runat="server" ControlToValidate="txtEditCity" ErrorMessage="*" SetFocusOnError="true" ForeColor="Red"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="State Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblStateName" runat="server" Text='<%# Eval("STATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Created Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreatedDate" runat="server" Text='<%# Eval("CREATEDDATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action" ShowHeader="False">
                                <EditItemTemplate>
                                    <asp:LinkButton ID="btnLinkUpdateCity" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="btnLinkCancel" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnLinkEdit" ImageUrl="~/Images/icons/edit_editor_pen_pencil_write-512.png" style="width: 23px;" runat="server" CausesValidation="False"  CommandName="Edit" Text="Edit"></asp:ImageButton>
                                    &nbsp;&nbsp;&nbsp;
                                    <asp:ImageButton ID="btnLinkDelete" ImageUrl="~/Images/icons/Recycle_Bin_Full.png" style="width: 23px;" OnClientClick="return confirm('Are you sure to delete this city?');" runat="server" CausesValidation="False" CommandName="Delete" Text="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

