﻿Imports System.Data
Imports HotelBAL
Imports HotelShared
Partial Class DetailsPort_Hotel_HoldHotelBooking

    Inherits System.Web.UI.Page
    Private ST As New HotelDAL.HotelDA()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            If Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx", False)
            End If

            If Not IsPostBack Then
                Dim grdds As New DataSet()
                'grdds = ST.GetHoldHotel("", "Hold", "")
                grdds = ST.HotelSearchRpt("01/01/2018 12:00:00 AM", "", "", "", "", "", "", "", "", "Hold", Session("UID").ToString, Session("User_Type").ToString())
                Bindgrid(grdds)
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Private Sub Bindgrid(ByVal ds As DataSet)
        Try
            Session("HoldGrdds") = ds
            GrdReport.DataSource = ds
            GrdReport.DataBind()
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Protected Sub GrdReport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdReport.PageIndexChanging
        Try
            GrdReport.PageIndex = e.NewPageIndex
            Bindgrid(Session("HoldGrdds"))
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub

    Protected Sub lnkCheckHotelStatus_Click(sender As Object, e As EventArgs)
        Try
            Dim lb As LinkButton = CType(sender, LinkButton)
            'Color Selected Grid View row
            Dim gvr As GridViewRow = TryCast(lb.Parent.Parent, GridViewRow)
            gvr.BackColor = System.Drawing.ColorTranslator.FromHtml("#F0F8FF")
            Dim HtlDetailsDs As New DataSet()
            HtlDetailsDs = ST.htlintsummary(lb.CommandArgument, "Ticket")

            If (HtlDetailsDs.Tables.Count > 0) Then
                Dim HotelDT As New DataTable()
                HotelDT = HtlDetailsDs.Tables(0)
                If HotelDT.Rows.Count > 0 Then
                    Dim HotelDetails As New HotelBooking()

                    HotelDetails.BookingID = HotelDT.Rows(0)("BookingID").ToString()
                    HotelDetails.Orderid = HotelDT.Rows(0)("OrderID").ToString()
                    HotelDetails.Provider = HotelDT.Rows(0)("Provider").ToString()
                    Dim objcancel As New HotelBAL.CommonHotelBL()
                    HotelDetails = objcancel.CheckHotelBookingStaus(HotelDetails)
                    Dim page As Page = TryCast(HttpContext.Current.Handler, Page)
                    If page IsNot Nothing Then
                        ScriptManager.RegisterStartupScript(page, page.[GetType](), "err_msg", "alert('" & HotelDetails.Status & "');", True)
                    End If

                End If
            End If
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
        End Try
    End Sub
End Class
