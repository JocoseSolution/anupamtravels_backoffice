﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false" CodeFile="PGReports.aspx.vb" Inherits="DetailsPort_Finance_PGReports" %>

<%@ Register Src="~/UserControl/AccountsControl.ascx" TagPrefix="uc1" TagName="Account" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#From").datepicker({ dateFormat: 'dd-mm-yy' }).val();
                $(".cd").click(function () {
                    $("#From").focus();
                }
                );
                $("#To").datepicker({ dateFormat: 'dd-mm-yy' }).val();
                $(".cd1").click(function () {
                    $("#To").focus();
                }
                );
            });
    </script>

    


    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="container-fluid" style="padding-right: 35px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">

                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">From Date</label>--%>
                                    <input type="text" placeholder="FROM DATE" name="From" id="From" class="form-control input-text full-width" readonly="readonly" />
                                    <span class="input-group-addon" style="background: #49cced">
                                        <span class="glyphicon glyphicon-calendar cd" style="cursor: pointer;"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">To Date</label>--%>
                                    <input type="text" placeholder="TO DATE" name="To" id="To" class="form-control input-text full-width" readonly="readonly" />
                                    <span class="input-group-addon" style="background: #49cced">
                                        <span class="glyphicon glyphicon-calendar cd1" style="cursor: pointer;"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">OrderId</label>--%>
                                    <asp:TextBox ID="txt_OrderId" placeholder="REF.ID" runat="server" CssClass="form-control input-text full-width"></asp:TextBox>
                                    <span class="input-group-addon" style="background: #49cced">
                                        <span class="fa fa-first-order"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2" id="td_Agency" runat="server">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Agency Name</label>--%>
                                    <input type="text" id="txtAgencyName" placeholder="AGENCY NAME" name="txtAgencyName" <%--onfocus="focusObj(this);"--%>
                                        <%--onblur="blurObj(this);" defvalue="Agency Name or ID"--%> autocomplete="off" <%--value="Agency Name or ID"--%> class="form-control input-text full-width" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                    <span class="input-group-addon" style="background: #49cced">
                                        <span class="fa fa-black-tie"></span>
                                    </span>

                                </div>
                            </div><div class="col-md-2">
                                <div class="form-group">
                                    <%--<label for="exampleInputEmail1">Status :</label>--%>
                                    <asp:DropDownList CssClass="input-text full-width" ID="drpPaymentStatus" runat="server">
                                        <asp:ListItem  value="" disabled selected style="display: none;">SELECT STATUS</asp:ListItem>
                                        <%--<asp:ListItem>STATUS</asp:ListItem>--%>
                                    </asp:DropDownList>
                                </div>
                            </div>
                                <div class="col-md-2">
                                <%--<label for="exampleInputEmail1">PaymentMode :</label>--%>
                                <asp:DropDownList CssClass="input-text full-width" ID="txtPaymentmode" runat="server">

                                    <asp:ListItem  value="" disabled selected style="display: none;">SELECT CARD TYPE</asp:ListItem>
                                    <%--<asp:ListItem Text="--select card Type---" Value="0"></asp:ListItem>--%>
                                    <asp:ListItem Text="CreditCard" Value="CC"></asp:ListItem>
                                    <asp:ListItem Text="DebitCard" Value="DC"></asp:ListItem>
                                    <asp:ListItem Text="NetBanking" Value="NB"></asp:ListItem>
                                    <%-- <asp:ListItem Text="CreditCard" Value="credit card"></asp:ListItem>
                                   <asp:ListItem Text="DebitCard" Value="debit card"></asp:ListItem>
                                  <asp:ListItem Text="NetBanking" Value="net banking"></asp:ListItem>   
                                  <asp:ListItem Text="Cash Card" Value="cash card"></asp:ListItem>   
                                  <asp:ListItem Text="Mobile Payment" Value="mobile payment"></asp:ListItem>   --%>
                                </asp:DropDownList>
                            </div>

                        </div>

                        <%--<div class="row">


                        

                        </div>--%>
                        <div class="row">
                            <div class="col-md-4" >
                                <div class="form-group">
                                    
                                    <asp:Button ID="btn_result" runat="server" Text="Find" Width="74px" CssClass="btn btn-success" />
                                    &nbsp;
                                    <%--                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <br />--%>
                                    <asp:Button ID="btn_export" runat="server" Text="Export To Excel" CssClass="btn btn-success" />
                                </div>
                            </div>
                        </div>
                        <%-- <div class="row">
                            <div style="color: #FF0000">
                                * N.B: To get Today's booking without above parameter,do not fill any field, only
                            click on search your booking.
                            </div>
                        </div>--%>

                        <div class="col-md-12">
                            <div id="divReport" runat="server" visible="true" style="background-color: #fff; overflow-y: scroll;" class="large-12 medium-12 small-12">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="grd_IntsaleRegis" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover" GridLines="None" PageSize="30" OnRowCommand="OnRowCommand" style="text-transform:uppercase;">
                                                        <Columns>

                                                            <%--  <asp:TemplateField HeaderText="Order Id">
                                                                <ItemTemplate>
                                                                    <a href='IntInvoiceDetails.aspx?OrderId=<%#Eval("OrderId")%>&amp;invno=<%#Eval("OrderId")%>&amp;tktno=<%#Eval("OrderId")%>&amp;AgentID=<%#Eval("AgentId")%>'
                                                                        style="color: #004b91; font-size: 11px; font-weight: bold" target="_blank">
                                                                        <asp:Label ID="OrderId" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>
                                                                       (Check Response )</a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>  --%>

                                                            <asp:TemplateField HeaderText="Check Status">
                                                                <ItemTemplate>
                                                                    <a href='CheckPGResponse.aspx?OrderId=<%#Eval("OrderId")%>&amp;AgentID=<%#Eval("AgentId")%>'
                                                                        rel="lyteframe" rev="width: 900px; height: 400px; overflow:hidden;"
                                                                        target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                                        <%--<asp:Label ID="OrderId" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>--%>                                                                       
                                                                        Check Status </a>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Order Id">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="OrderId" runat="server" Text='<%#Eval("ORDER_ID")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="UserId">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="AgentId" runat="server" Text='<%#Eval("USER_ID")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Brand Id">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="BrandId" runat="server" Text='<%#Eval("BRAND_ID")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Brand NAME">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Brand_Name" runat="server" Text='<%#Eval("BRAND_NAME")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Status">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Status" runat="server" Text='<%#Eval("STATUS")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="UnmappedStatus">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="UnmappedStatus" runat="server" Text='<%#Eval("UNMAPPED_STATUS")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Api Status">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ApiStatus" runat="server" Text='<%#Eval("API_STATUS")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Tracking Id">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Trackingid" runat="server" Text='<%#Eval("TRACKING_ID")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Bank RefNo">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="BankRefNo" runat="server" Text='<%#Eval("BANK_REF._NO.")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="TId">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TId" runat="server" Text='<%#Eval("TId")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Original Amount">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="OriginalAmount" runat="server" Text='<%#Eval("ORIGINAL_AMOUNT")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Convenience Fee">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TotalCharges" runat="server" Text='<%#Eval("TOTAL_CHARGES")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Dis. Amount">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="DiscountValue" runat="server" Text='<%#Eval("DISCOUNT_VALUE")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Merchant Amount">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="MerAamount" runat="server" Text='<%#Eval("MER_AMOUNT")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Total Amount">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TotalAmount" runat="server" Text='<%#Eval("AMOUNT")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Wallet-Update">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CreditLimitUpdate" runat="server" Text='<%#Eval("CREDIT_LIMIT_UPDATE")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Error Message">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ErrorText" runat="server" Text='<%#Eval("ERROR_TEXT")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Service Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="ServiceType" runat="server" Text='<%#Eval("SERVICE_TYPE")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Payment Mode">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_paxtype" runat="server" Text='<%#Eval("PAYMENT_MODE")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Card/Bank Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="CardName" runat="server" Text='<%#Eval("CARD_NAME")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Created Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_CDate" runat="server" Text='<%#Eval("CREATED_DATE")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <%--<asp:ButtonField CommandName="ButtonField" Text="Check-Status" ButtonType="Button" />--%>
                                                        </Columns>
                                                        <RowStyle CssClass="RowStyle" />
                                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                        <PagerStyle CssClass="PagerStyle" />
                                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                        <HeaderStyle CssClass="HeaderStyle" />
                                                        <EditRowStyle CssClass="EditRowStyle" />
                                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div id="DivPrint" runat="server" visible="true"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

</asp:Content>





