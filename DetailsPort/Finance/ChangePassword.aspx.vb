﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class DetailsPort_Finance_ChangePassword

    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblmsg.Text = ""

        CType(Page.Master.FindControl("lblBC"), Label).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Flight Setting </a><a class='current' href='#'>Change Password</a>"

    End Sub

    Protected Sub ButtonSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSubmit.Click
        Dim result As Integer = 0
        Try
            Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)

            'Checking if old password exist in to database table

            Dim cmdPass As SqlCommand
            Dim effect As Byte
            cmdPass = New SqlCommand("select * from ExecuRegister  where user_id='" + Convert.ToString(Session("UID")) + "' ", con)
            If con.State = ConnectionState.Open Then
                con.Close()
            End If



            con.Open()
            Dim rd As SqlDataReader = cmdPass.ExecuteReader()
            Do While rd.Read
                If txtOldPassword.Text = rd("password") Then
                    effect = 1
                End If
            Loop
            rd.Close()
            con.Close()
            cmdPass.Dispose()

            'End process to check old password

            If effect = 1 Then
                Dim cmd As New SqlCommand
                cmd.CommandText = "usp_changePassword_PP"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add("@userID", SqlDbType.NVarChar, 500).Value = Session("UID").ToString()
                cmd.Parameters.Add("@oldPassword", SqlDbType.NVarChar, 100).Value = txtOldPassword.Text.Trim()
                cmd.Parameters.Add("@Password", SqlDbType.NVarChar, 500).Value = TextBoxNewPass.Text.Trim()
                cmd.Connection = con
                con.Open()
                result = Convert.ToInt32(cmd.ExecuteScalar())
                con.Close()
                cmd.Dispose()
            Else
                lblmsg.Text = "Old password not match,Try again!"
                lblmsg.ForeColor = System.Drawing.Color.Red
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

        If (result > 0) Then
            lblmsg.Text = "Password Changed sucessfully."
            lblmsg.ForeColor = System.Drawing.Color.Green
            'Else
            '    lblmsg.Text = " unable to change Password, Please try after some time."
        End If

    End Sub

End Class
