﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;
using System.Data;


public partial class Dashboard : System.Web.UI.Page
{
    string cs = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            getnotificationstatus();

            GetInfo();
            BindCount();
            GetChartData();
            GetChartTypes();
        }
    }




    public void getnotificationstatus()
    {
        try
        {
            DataSet dst = new DataSet();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            DataTable dt5 = new DataTable();
            SqlDataAdapter sdr = new SqlDataAdapter("sp_getnotification_status2", cs);
            sdr.SelectCommand.CommandType = CommandType.StoredProcedure;
            sdr.Fill(dst);
            dt1 = dst.Tables[0];
            dt2 = dst.Tables[1];
            dt3 = dst.Tables[2];
            dt4 = dst.Tables[3];
            dt5 = dst.Tables[4];



            lbticketed.Text = dt1.Rows.Count.ToString();
            lbpending.Text = dt2.Rows.Count.ToString();
            lbrefund.Text = dt3.Rows.Count.ToString();
            lbreissue.Text = dt4.Rows.Count.ToString();

            Session["OrderId"] = dt5.Rows[0]["Orderid"].ToString();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }

    private void GetInfo()
    {
        try
        {
            using (SqlConnection con = new SqlConnection(cs))
            {

                // Query to retrieve the 2 columns (1 column for X-AXIS and the other for Y-AXIS)
                // of data required for the chart control
                SqlCommand cmd = new SqlCommand("select count(*) as TotalCust from agent_register where agent_status ='ACTIVE'", con);

                con.Open();
                SqlDataAdapter Sdr = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                Sdr.Fill(dt);
                totalcust.Text = dt.Rows[0]["TotalCust"].ToString();

                lbl_orderid.Text = Session["OrderId"].ToString();

            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    private void GetChartData()
    {
        string cs = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(cs))
        {

            // Query to retrieve the 2 columns (1 column for X-AXIS and the other for Y-AXIS)
            // of data required for the chart control
            SqlCommand cmd = new SqlCommand("sp_chart", con);
            con.Open();
            SqlDataAdapter Sdr = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            Sdr.Fill(dt);
            //SqlDataReader rdr = cmd.ExecuteReader();
            // Pass the datareader object that contains the chart data and specify the column
            // to be used for X-AXIS values. The other column values will be automatically
            // used for Y-AXIS
            //Chart1.DataBindTable(rdr, "StudentName");

            // Retrieve the Series to which we want to add DataPoints

            //Series series = Chart1.Series["Series1"];

            ////Chart1.ChartAreas["ChartArea1"].AxisX.Maximum = 12;
            //// Loop thru each Student record
            //while (rdr.Read())
            //{
            //    // Add X and Y values using AddXY() method

            //    series.Points.AddXY(rdr["CreateDate"].ToString(), rdr["TotalBookingCost"]);
            //}
            Chart1.DataSource = dt;
            Chart1.Series["Series1"].XValueMember = "CreateDate";
            Chart1.Series["Series1"].YValueMembers = "TotalBookingCost";
            Chart1.DataBind();
        }
    }

    //        Chart1.DataSource = rdr;
    //        Chart1.DataBind();
    //    }
    //}

    private void GetChartTypes()
    {
        foreach (int chartType in Enum.GetValues(typeof(SeriesChartType)))
        {
            ListItem li = new ListItem(Enum.GetName(typeof(SeriesChartType), chartType), chartType.ToString());
            //DropDownList1.Items.Add(li);
        }
    }

    //protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    GetChartData();
    //    // Retrieve the series by index and then set the ChartType property value
    //    //this.Chart1.Series[0].ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), DropDownList1.SelectedValue);
    //    // Alternatively, FirstOrDefault() linq method can also be used
    //    // this.Chart1.Series.FirstOrDefault().ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), DropDownList1.SelectedValue);
    //}





    protected void BindCount()
    {
        try
        {
            if (Session["User_Type"].ToString() == "SALES")
            {
                Response.Redirect("DetailsPort/Admin/Agent_Details.aspx", false);
            }
            DataSet ds = new DataSet();
            string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);
            con.Open();

            SqlCommand cmd = new SqlCommand("usp_Get_Ticket_Satus_wise_Count");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@startDate", "2016-06-15 15:15:23.210");
            cmd.Parameters.AddWithValue("@endDate", "2016-06-15 15:15:23.210");
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.Connection = con;
            sda.SelectCommand = cmd;

            DataTable dt = new DataTable();
            sda.Fill(ds);
            dt = ds.Tables[0];

            lbticketed.Text = Convert.ToString(dt.Rows[0]["TicketCount"].ToString());
            //lblhold.Text = Convert.ToString(ds.Tables[0].Rows[0]["HoldNum"].ToString());
            //lblreissueReq.Text = Convert.ToString(dt.Rows[0]["reissueReq"].ToString());
            //lblCancelReq.Text = Convert.ToString(dt.Rows[0]["CancelReq"].ToString());
            //lbltktreq.Text = Convert.ToString(dt.Rows[0]["TKTREQ"].ToString());
            //lblprehold.Text = Convert.ToString(dt.Rows[0]["PreHold"]);

            lbpending.Text = Convert.ToString(ds.Tables[0].Rows[0]["BSCOUNT"].ToString());
            //lblbshold.Text = Convert.ToString(dt.Rows[0]["BSHOLD"].ToString());
            //lblbscancel.Text = Convert.ToString(dt.Rows[0]["BSCANCEL"].ToString());
            //lblbsrequst.Text = Convert.ToString(dt.Rows[0]["BSREQ"].ToString());


            //lblhtlcount.Text = Convert.ToString(ds.Tables[0].Rows[0]["HTLCOUNT"].ToString());
            ////lblhtlhold.Text = Convert.ToString(dt.Rows[0]["HTLHOLD"].ToString());
            ////lblhtlcancel.Text = Convert.ToString(dt.Rows[0]["HTLCANCEL"].ToString());
            ////lblhtlrequest.Text = Convert.ToString(dt.Rows[0]["HTLREQ"].ToString());

            //DataList1.DataSource = ds.Tables[1];
            //DataList1.DataBind();

            //DataList2.DataSource = ds.Tables[2];
            //DataList2.DataBind();

            //DataList3.DataSource = ds.Tables[3];
            //DataList3.DataBind();


            //DataList4.DataSource = ds.Tables[4];
            //DataList4.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }

}