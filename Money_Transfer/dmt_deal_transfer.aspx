﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="dmt_deal_transfer.aspx.cs" Inherits="Money_Transfer_dmt_deal_transfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Deal Transfer From :</label>
                                    <asp:DropDownList ID="ddl_ptype_From" CssClass="form-control" runat="server" TabIndex="2"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Deal Transfer To :</label>
                                    <asp:DropDownList ID="ddl_ptype_To" CssClass="form-control" runat="server" TabIndex="2"></asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Button ID="Submit" runat="server" Text="Submit" OnClick="Submit_Click" OnClientClick="return Validate()" />
                                </div>
                            </div>
                        </div>

                        <div class="clear"></div>
                        <div id="DD" style="color: red">*Note: Your DealTransferFrom as you Select which will be insert into DealTransferTo and Previous Deal of DealTransferTo Will be Deleted</div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function Validate() {
            var ddl_ptype = document.getElementById("<%=ddl_ptype_From.ClientID %>");
            var ddl_ptype_To = document.getElementById("<%=ddl_ptype_To.ClientID %>");

            if (ddl_ptype.value == 0) {               
                alert("Please select GroupType From!");
                return false;
            }

            if (ddl_ptype_To.value == 0) {               
                alert("Please select GroupType To!");
                return false;
            }
        }
    </script>
</asp:Content>

