﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Money_Transfer_dmt_report : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlDataAdapter adap;
    protected SqlTransactionDom STDom = new SqlTransactionDom();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            Response.Redirect("~/Login.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                Label lblBc = (Label)Master.FindControl("lblBC");
                lblBc.Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Money Transfer </a><a class='current' href='#'>Report </a></a>"; ;
                BindDMTTranctionReport();
                BindDmtStatus();
            }
        }
    }

    protected void BindDMTTranctionReport()
    {
        DataTable dtRecord = GetDmtTrancReportList(null, null, null, null, null, null, null);
        BindListView(dtRecord);
    }

    private void BindListView(DataTable dtRecord)
    {
        if (dtRecord != null && dtRecord.Rows.Count > 0)
        {
            Session["TransRecord"] = dtRecord;
            PagingButtom.Visible = true;
        }
        else
        {
            Session["TransRecord"] = null;
            PagingButtom.Visible = false;
        }

        lstTransaction.DataSource = dtRecord;
        lstTransaction.DataBind();
    }

    private DataTable GetDmtTrancReportList(string agencyid = null, string fromDate = null, string toDate = null, string mobile = null, string trackid = null, string orderId = null, string status = null)
    {
        DataTable dtDueReport = new DataTable();

        try
        {
            string newfromdate = string.Empty; string newtodate = string.Empty;

            if (!string.IsNullOrEmpty(fromDate))
            {
                newfromdate = String.Format(fromDate.Split('-')[2], 4) + "-" + String.Format(fromDate.Split('-')[1], 2) + "-" + String.Format(fromDate.Split('-')[0], 2);
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                //newtodate = String.Format(fromDate.Split('-')[2], 4) + "-" + String.Format(fromDate.Split('-')[1], 2) + "-" + String.Format(fromDate.Split('-')[0], 2);
				 newtodate = String.Format(toDate.Split('-')[2], 4) + "-" + String.Format(toDate.Split('-')[1], 2) + "-" + String.Format(toDate.Split('-')[0], 2);
            }

            adap = new SqlDataAdapter("sp_GetMoneyTransDetails", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@AgentId", !string.IsNullOrEmpty(agencyid) ? agencyid.Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@RemtMobile", !string.IsNullOrEmpty(mobile) ? mobile.Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@txnId", !string.IsNullOrEmpty(orderId) ? orderId.Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@TrackId", !string.IsNullOrEmpty(trackid) ? trackid.Trim() : null);
            adap.SelectCommand.Parameters.AddWithValue("@FromDate", !string.IsNullOrEmpty(newfromdate) ? newfromdate.Trim() : "");
            adap.SelectCommand.Parameters.AddWithValue("@ToDate", !string.IsNullOrEmpty(newtodate) ? newtodate.Trim() + " 23:59:59" : null);
            adap.SelectCommand.Parameters.AddWithValue("@Status", !string.IsNullOrEmpty(status) ? status.Trim() : null);
            adap.Fill(dtDueReport);

        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return dtDueReport;
    }

    protected void btn_result_Click(object sender, EventArgs e)
    {
        string status = ddlstatus.SelectedValue;
        BindListView(GetDmtTrancReportList(Request["hidtxtAgencyName"].ToString(), Request["From"].ToString(), Request["To"].ToString(), txt_Mobile.Text, txt_Trackid.Text, txt_OrderId.Text, status));
    }

    private void BindDmtStatus()
    {
        DataTable dtStatus = new DataTable();

        adap = new SqlDataAdapter("sp_GetDmtStatus", con);
        adap.SelectCommand.CommandType = CommandType.StoredProcedure;
        adap.Fill(dtStatus);

        if (dtStatus != null && dtStatus.Rows.Count > 0)
        {
            ddlstatus.DataSource = dtStatus;
            ddlstatus.DataBind();

            ddlstatus.DataTextField = "status";
            ddlstatus.DataValueField = "status";
            ddlstatus.DataBind();
            ddlstatus.Items.Insert(0, new ListItem("All", ""));
        }
    }

    protected void lstTransaction_PagePropertiesChanged(object sender, EventArgs e)
    {
        string status = ddlstatus.SelectedValue;
        BindListView(Session["TransRecord"] as DataTable);
    }
}