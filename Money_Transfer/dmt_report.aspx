﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="dmt_report.aspx.cs" Inherits="Money_Transfer_dmt_report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="<%=ResolveUrl("~/Hotel/css/B2Bhotelengine.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }

        .tooltip1 {
            position: relative;
        }

        .tooltiptext {
            visibility: hidden;
            width: 120px;
            background-color: black;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            /* Position the tooltip */
            position: absolute;
            z-index: 1;
        }

        #tooltip {
            z-index: 9999;
            position: absolute;
            top: 200px;
            float: right;
            padding: 5px;
            right: 280px;
            border: 2px solid #04034f;
            background-color: #fff;
            width: auto;
            min-width: 300px;
        }

        .table .table {
            background-color: #fff;
            border: 1px solid #ccc;
        }

        .tooltip1:hover .tooltiptext {
            visibility: visible;
        }

        .popupnew2 {
            position: absolute;
            top: 10px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .hovercolor {
            font-weight: bold;
            color: #004b91;
            font-size: 11px;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: auto !important;
            overflow-y: auto !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }

        .pagination {
            margin: 0;
            /*margin-bottom: 30px;*/
        }

            .pagination > span > a, .pagination > span > span {
                background-color: #fff;
                border: 1px solid #ddd;
                display: inline;
                float: left;
                line-height: 1.42857;
                margin-left: -1px;
                padding: 6px 12px;
                position: relative;
                text-decoration: none;
            }

            .pagination > span > a {
                color: #E7302A !important;
            }

            .pagination > span > span {
                color: #fff !important;
            }

                .pagination > span > a:first-child, .pagination > span > span:first-child {
                    border-bottom-left-radius: 4px;
                    border-top-left-radius: 4px;
                    margin-left: 0;
                    color: #E7302A !important;
                }

                .pagination > span > a:last-child, .pagination > span > span:last-child {
                    border-bottom-right-radius: 4px;
                    border-top-right-radius: 4px;
                    color: #E7302A !important;
                }

            .pagination > span > span {
                background-color: #E7302A !important;
                color: #FFF;
                cursor: default;
                z-index: 2;
            }

            .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
                z-index: 3;
                color: #fff;
                cursor: default;
                background-color: #00c2ff !important;
                border-color: #00c2ff !important;
                cursor: pointer;
            }

            .pagination > li > a, .pagination > li > span {
                cursor: pointer;
            }

        th {
            text-align: center;
        }

        td {
            color: #000;
            text-align: center;
        }

        .form-control {
            height: 33px !important;
        }

        .ui-datepicker .ui-datepicker-next:before {
    border-left: none!important; 
}
        .ui-datepicker .ui-datepicker-prev:before {
    border-left: none!important; 
}
        .table td {
    text-align: center!important;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="row form-group">
                            <div class="col-sm-3">
                                <div class="form-group" id="td_Agency" runat="server">
                                    <label for="exampleInputPassword1">Agency Name</label>
                                    <input type="text" id="txtAgencyName" class="form-control input-text full-width" name="txtAgencyName" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>From</label>
                                <input type="text" name="From" id="From" placeholder="Select Date" class="form-control input-text full-width" readonly="readonly" />
                            </div>
                            <div class="col-md-3">
                                <label>To</label>
                                <input type="text" name="To" placeholder="Select Date" id="To" class="form-control input-text full-width" readonly="readonly" />
                            </div>
                            <div class="col-md-3">
                                <label>Transaction Number</label>
                                <asp:TextBox ID="txt_OrderId" placeholder="Enter Transaction Number" class="form-control input-text full-width" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-3">
                                <label>TrackID</label>
                                <asp:TextBox ID="txt_Trackid" placeholder="Enter trackid" class="form-control input-text full-width" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <label>Remitter Mobile</label>
                                <asp:TextBox ID="txt_Mobile" placeholder="Enter Remitter Mobile" MaxLength="10" class="form-control input-text full-width" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <label>Status</label>
                                <asp:DropDownList class="form-control" ID="ddlstatus" runat="server">
                                    <asp:ListItem Value="">All</asp:ListItem>
                                    <%--<asp:ListItem Value="success">Success</asp:ListItem>
                                    <asp:ListItem Value="process">Under Process</asp:ListItem>
                                    <asp:ListItem Value="fail">Failed</asp:ListItem>--%>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-3">
                                <br />
                                <asp:Button ID="btn_result" runat="server" CssClass="button buttonBlue form-control" Text="Search Result" OnClick="btn_result_Click" />
                            </div>
                        </div>
                        <div class="row form-group" style="font-size: 15px; line-height: 20px; text-align: justify; color: Red;">
                            * N.B: To get Today's transaction without above parameter,do not fill any field, only click on search your transaction.
                        </div>

                        <div class="clear"></div>
                        <div class="row form-group">
                            <div class="col-md-12 form-group">
                                <div class="row table-responsive text-nowrap">
                                    <table class="table" data-toggle="table" style="width: 100%!important">
                                        <asp:ListView ID="lstTransaction" runat="server" OnPagePropertiesChanged="lstTransaction_PagePropertiesChanged">
                                            <LayoutTemplate>
                                                <tr>
                                                    <th></th>
                                                    <th>TxnDate</th>
                                                    <th>Agent ID</th>
                                                     <th>Agency</th>
                                                    <th>Transaction Number</th>
                                                    <th>TrackID </th>
                                                    <th>Mode</th>
                                                    <th>Amount</th>
                                                   <%-- <th>Charge</th>--%>
                                                    <th>Sender Mobile</th>
                                                    <th>Sender</th>
                                                    <th>Reciver</th>
                                                    <th>Status</th>
                                                </tr>
                                                <div runat="server" id="ItemPlaceholder"></div>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><a target="_blank" href="https://anupamtravelonline.com/dmt-manager/printtrans.aspx?mobile=<%#Eval("Mobile")%>&beneficaryid=<%#Eval("Beneficiary_Id")%>&trackid=<%#Eval("TrackId")%>&agentid=<%#Eval("AgentId")%>" title="print" style="color: #fff; font-weight: bold; border: 1px solid red; padding: 2px; background: red; cursor: pointer;">Print</a></td>
                                                    <td><%# Eval("updatedDate") %></td>
                                                    <td><%# Eval("AgentId") %></td>
                                                    <td><%# Eval("Agency_Name") %></td>
                                                    <td><%# Eval("txnId") %></td>
                                                    <td><%# Eval("TrackId") %></td>
                                                    <td><%# Eval("channel") %></td>
                                                    <td><%# Eval("amount") %></td>
                                                    <%--<td><%# Eval("charged_amt") %></td>--%>
                                                    <td style="color: red; font-weight: bold;"><%#Eval("Mobile")%> </td>
                                                    <td>
                                                        <asp:Label ID="RemitterName" runat="server" class="gridViewToolTip hovercolor" Text='<%#Eval("FirstName")%>'>&nbsp;<span class="fa fa-info-circle"></span></asp:Label>
                                                        <div id="tooltip1" style="display: none;">
                                                            <div>
                                                                <table width="100%" cellpadding="11" cellspacing="11" border="0" class="table table-border" style="border: 1px solid #ccc; margin-top: 6px;">
                                                                    
                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Sender Name:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%#Eval("SenderName") %>                                                                         
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Mobile Number:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%#Eval("Mobile") %>
                                                                        </td>
                                                                    </tr> 
                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Address:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%#Eval("CurrentAddress") %>
                                                                        </td>
                                                                    </tr>     
                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Pincode:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%#Eval("PinCode") %>
                                                                        </td>
                                                                    </tr>                                                                                                                                    
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="BeneficaryName" runat="server" class="gridViewToolTip hovercolor" Text='<%#Eval("bene_Name")%>'>&nbsp;<span class="fa fa-info-circle"></span></asp:Label>
                                                        <div id="tooltip" style="display: none;">
                                                            <div>
                                                                <table width="100%" cellpadding="11" cellspacing="11" border="0" class="table table-border" style="border: 1px solid #ccc; margin-top: 6px;">
                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">BenificieryId:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%# Eval("Beneficiary_Id") %>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Name:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%# Eval("bene_Name") %>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Account:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%# Eval("accountNumber") %>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Ifsc:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%# Eval("ifscCode") %>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px; font-weight: bold;">Bank:
                                                                        </td>
                                                                        <td style="width: 166px;">
                                                                            <%# Eval("bankName") %>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><%# Eval("status").ToString() %></td>
                                                </tr>
                                            </ItemTemplate>
                                            <EmptyDataTemplate>
                                                <table class="table" data-toggle="table" style="width: 100%!important">
                                                    <tr>
                                                        <th></th>
                                                    <th>TxnDate</th>
                                                    <th>Agent ID</th>
                                                    <th>Transaction Number</th>
                                                    <th>TrackID </th>
                                                    <th>Mode</th>
                                                    <th>Amount</th>
                                                   <%-- <th>Charge</th>--%>
                                                    <th>Sender Mobile</th>
                                                    <th>Sender</th>
                                                    <th>Reciver</th>
                                                    <th>Status</th>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="9" style="color: red; text-align: center;">NO RECORD FOUND !</td>
                                                    </tr>
                                                </table>
                                            </EmptyDataTemplate>
                                        </asp:ListView>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <ul class="pagination">
                                    <asp:DataPager runat="server" ID="PagingButtom" PagedControlID="lstTransaction" PageSize="15">
                                        <Fields>
                                            <asp:NextPreviousPagerField FirstPageText="First" PreviousPageText="&laquo;" ShowFirstPageButton="false" ShowNextPageButton="false" ShowPreviousPageButton="true" RenderDisabledButtonsAsLabels="false" />
                                            <asp:NumericPagerField ButtonCount="5" />
                                            <asp:NextPreviousPagerField LastPageText="Last" NextPageText="&raquo;" ShowLastPageButton="false" ShowNextPageButton="true" ShowPreviousPageButton="false" RenderDisabledButtonsAsLabels="false" />
                                        </Fields>
                                    </asp:DataPager>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript">
        $(function () {
            InitializeToolTip();
        });
    </script>
</asp:Content>

