﻿Imports System.Data

Partial Class AirInt_PriceDetails
    Inherits System.Web.UI.Page
    Dim objSelectedfltCls As New clsInsertSelectedFlight
    Dim objFareBreakup As New clsCalcCommAndPlb
    Dim objDA As New SqlTransaction

    Dim DomAirDt As DataTable
    Dim trackId As String, LIN As String
    Dim FT As String = ""
    Dim Adult As Integer
    Dim Child As Integer
    Dim Infant As Integer
    Dim SelectedFltArray As Array
    Dim strFlt As String = "", strFare As String = ""
    Dim fareHashtbl As Hashtable
    Dim STDom As New SqlTransactionDom()
    'varaibles
    Dim VCOB As String = "", VCIB As String = ""
    Dim TripOB As String = "", TripIB As String = ""
    Dim ATOB As String = "", ATIB As String = ""
    Dim FLT_STAT As String = ""
    Dim IntAirDt As DataTable
    Private ObjIntDetails As New IntlDetails()
    Dim objSql As New SqlTransactionNew
    Dim TransTD As String = ""
    Dim dtpnr As New DataTable() 'Header
    Dim dtpax As New DataTable() 'Pax
    Dim dtfare As New DataTable()
    Dim OBFltDs, IBFltDs As DataSet
    Dim SSR_LOG As DataSet
    Dim objUMSvc As New FltSearch1()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try


            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("../Login.aspx")
            Else
                If Not Page.IsPostBack Then


                    If HttpContext.Current.Request.QueryString.Count = 1 Then
                        trackId = HttpContext.Current.Request.QueryString(0).ToString()
                        OBFltDs = objDA.GetFltDtls(trackId, Session("UID"))
                        IntAirDt = OBFltDs.Tables(0)
                        ViewState("trackid") = trackId
                    End If

                    If IntAirDt.Rows(IntAirDt.Rows.Count - 1)("TripType") = "R" Then
                        FT = "R"
                    Else
                        FT = "O"
                    End If

                    Session("IntAirDt") = IntAirDt
                    Adult = Convert.ToInt16(OBFltDs.Tables(0).Rows(0)("Adult"))
                    Child = Convert.ToInt16(OBFltDs.Tables(0).Rows(0)("Child"))
                    Infant = Convert.ToInt16(OBFltDs.Tables(0).Rows(0)("Infant"))
                    ' Now Show All Details in Divs
                    ' Flight Details Both in 1 Div
                    Try
                        'SelectedFltArray = IntAirDt.Select("TripType='O'", "")
                        'strFlt = "<div class='w45 lft padding2s'><div class='f18'>OutBound</div><div class='clear'></div><div class='hr'></div><div class='clear'></div>"
                        'For i As Integer = 0 To SelectedFltArray.Length - 1
                        '    strFlt = strFlt & "<div>"
                        '    strFlt = strFlt & "<div class='w24 padding1 lft'><div class='f16'>" & (SelectedFltArray(i)("DepartureLocation")) & " - " & (SelectedFltArray(i)("ArrivalLocation")) & "</div><div>" & (SelectedFltArray(i)("Departure_Date")) & "</div></div>"
                        '    strFlt = strFlt & "<div class='w40 padding1 lft'><div class='f16'>" & (SelectedFltArray(i)("MarketingCarrier")) & "-" & (SelectedFltArray(i)("FlightIdentification")) & "</div><div>Class: " & (SelectedFltArray(i)("RBD")) & "</div></div>"
                        '    strFlt = strFlt & "<div class='w28 padding1 lft'><div><span class='lft w30'>Dep:</span>" & (SelectedFltArray(i)("DepartureTime")) & "Hrs.</div><div><span class='lft w30'>Arr:</span> " & (SelectedFltArray(i)("ArrivalTime")) & " Hrs</div>"
                        '    strFlt = strFlt & "</div>"
                        'Next
                        'strFlt = strFlt & "</div>"
                        'If FT = "R" Then
                        '    SelectedFltArray = IntAirDt.Select("TripType='R'", "")
                        '    strFlt = strFlt & "<div class='padding2s w45 rgt'><div class='f18'>InBound</div><div class='clear'></div><div class='hr'></div><div class='clear'></div>"
                        '    For i As Integer = 0 To SelectedFltArray.Length - 1
                        '        strFlt = strFlt & "<div>"
                        '        strFlt = strFlt & "<div class='w24 padding1 lft'><div class='f16'>" & (SelectedFltArray(i)("DepartureLocation")) & " - " & (SelectedFltArray(i)("ArrivalLocation")) & "</div><div>" & (SelectedFltArray(i)("Departure_Date")) & "</div></div>"
                        '        strFlt = strFlt & "<div class='w40 padding1 lft'><div class='f16'>" & (SelectedFltArray(i)("MarketingCarrier")) & "-" & (SelectedFltArray(i)("FlightIdentification")) & "</div><div>Class: " & (SelectedFltArray(i)("RBD")) & "</div></div>"
                        '        strFlt = strFlt & "<div class='w28 padding1 lft'><div><span class='lft w30'>Dep:</span>" & (SelectedFltArray(i)("DepartureTime")) & "Hrs</div><div><span class='lft w30'>Arr:</span> " & (SelectedFltArray(i)("ArrivalTime")) & "Hrs</div></div>"
                        '        strFlt = strFlt & "</div>"
                        '    Next
                        '    strFlt = strFlt & "</div>"
                        'End If
                        'divFltDtls.InnerHtml = strFlt
                        divFltDtls.InnerHtml = "<div class='brdr'><div class='bld' style='padding-left:10px'>" & STDom.CustFltDetails_Intl(OBFltDs) & "</div></div>" 'STDom.CustFltDetails_Intl(OBFltDs)
                    Catch ex As Exception

                    End Try

                    divFareDtls.InnerHtml = "<div class='brdr'><div class='f16 bgf1 bld lh30' style='color:#fff'>&nbsp; Fare Information</div><div class='clear'></div><div class='hr'></div><div class='clear'></div><div>" & fareBreakupfun2(IntAirDt, Adult, Child, Infant) & "</div></div>" '"<div class='clear1'></div><div class='padding1 w100 lft'><div class='bld f13'></div><div>" & fareBreakupfun2(IntAirDt, Adult, Child, Infant) & "</div></div><div class='clear1'></div>"
                    divPaxdetails.InnerHtml = showPaxDetails(trackId, FT)

                Else
                    'Page Post Back

                End If

            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

    End Sub

    Private Function fareBreakupfun2(ByVal IntAirDt As DataTable, ByVal Adult As Integer, ByVal Child As Integer, ByVal Inf As Integer) As String
        Try

            Dim OrderId As String = IntAirDt.Rows(0)("Track_id").ToString()
            Dim VC = IntAirDt.Rows(0)("ValiDatingCarrier")
            Dim MBDT As DataSet = objSql.Get_MEAL_BAG_FareDetails(OrderId, "")
            Dim MBPR As Decimal = 0, MealPr As Decimal = 0, BgPr As Decimal = 0
            If (MBDT.Tables(0).Rows.Count > 0) Then
                For jj As Integer = 0 To MBDT.Tables(0).Rows.Count - 1
                    MealPr = MealPr + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("MealPrice"))
                    BgPr = BgPr + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("BaggagePrice"))
                    MBPR = MBPR + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("MealPrice")) + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("BaggagePrice"))
                Next
                'IF Header is Updated then Only Allow Booking
                SSR_LOG = objSql.GetSSR_Log_Detail(OrderId)
                If (Convert.ToDecimal(SSR_LOG.Tables(0).Rows(0)("Header_TBC")) + MBPR) <= Convert.ToDecimal(IntAirDt.Rows(0)("TotFare").ToString()) Then
                    Submit.Visible = True
                Else
                    'Submit.Visible = False
                    Response.Redirect("../FlightInt/BookingMsg.aspx?msg=2", False)
                End If
            End If
            Dim tax(), tax1() As String, yq As Integer = 0, tx As Integer = 0
            tax = IntAirDt.Rows(0)("Adt_Tax").ToString.Split("#")
            For i As Integer = 0 To tax.Length - 2
                If InStr(tax(i), "YQ") Then
                    tax1 = tax(i).Split(":")
                    yq = yq + Convert.ToInt32(Math.Round(Convert.ToDecimal(tax1(1).ToString()), 0))
                Else
                    tax1 = tax(i).Split(":")
                    tx = tx + Convert.ToInt32(Math.Round(Convert.ToDecimal(tax1(1).ToString()), 0))
                End If
            Next
            'Changed ID 02/04/2014
            strFare = strFare & "<div id='OB_FT' border='0' class='large-9 medium-9 small-12 columns'>"



            strFare = strFare & "<div class='large-9 medium-9 small-12 columns'>"

            strFare = strFare & "<div class='large-2 medium-2 small-2 columns bld'>Pax Type</div>"
            strFare = strFare & "<div class='large-2 medium-2 small-2 columns bld'>Base Fare</div>"
            strFare = strFare & "<div class='large-2 medium-3 small-3 columns bld'>Fuel Charge</div>"
            strFare = strFare & "<div class='large-2 medium-2 small-2 columns bld'>Tax </div>"
            strFare = strFare & "<div class='large-2 medium-2 small-2 columns bld'>Total</div>"
            strFare = strFare & "</div>"
            strFare = strFare & "<div class='clear'></div>"


            strFare = strFare & "<div class='large-9 medium-9 small-12 columns'>"
            strFare = strFare & "<div class='large-2 medium-2 small-2 columns'>ADULT</div>"
            strFare = strFare & "<div class='large-2 medium-2 small-2 columns'>" & IntAirDt.Rows(0)("AdtBFare") & "</div>"
            strFare = strFare & "<div class='large-2 medium-3 small-3 columns'>" & yq & "</div>"
            strFare = strFare & "<div class='large-2 medium-2 small-2 columns'>" & tx & "</div>"
            strFare = strFare & "<div class='large-2 medium-2 small-2 columns'>" & IntAirDt.Rows(0)("AdtFare") & "&nbsp;<b>x</b>" & Adult & "</div>"

            strFare = strFare & "</div>"
            strFare = strFare & "<div class='clear'></div>"

            If Child > 0 Then
                Try
                    yq = 0
                    tx = 0
                    'tax = fareHashtbl("ChdTax").ToString.Split("#")
                    tax = IntAirDt.Rows(0)("Chd_Tax").ToString.Split("#")
                    For i As Integer = 0 To tax.Length - 2
                        If InStr(tax(i), "YQ") Then
                            tax1 = tax(i).Split(":")
                            yq = yq + Convert.ToInt32(Math.Round(Convert.ToDecimal(tax1(1).ToString()), 0))
                        Else
                            tax1 = tax(i).Split(":")
                            tx = tx + Convert.ToInt32(Math.Round(Convert.ToDecimal(tax1(1).ToString()), 0))
                        End If
                    Next
                Catch ex As Exception
                    clsErrorLog.LogInfo(ex)
                End Try

                strFare = strFare & "<div class='large-9 medium-9 small-12 columns'>"
                strFare = strFare & "<div class='large-2 medium-2 small-2 columns'>CHILD</div>"
                strFare = strFare & "<div class='large-2 medium-2 small-2 columns'>" & IntAirDt.Rows(0)("ChdBFare") & "</div>"
                strFare = strFare & "<div class='large-2 medium-3 small-3 columns'>" & yq & "</div>"
                strFare = strFare & "<div class='large-2 medium-2 small-2 columns'>" & tx & "</div>"
                strFare = strFare & "<div class='large-2 medium-2 small-2 columns'>" & IntAirDt.Rows(0)("ChdFare") & "&nbsp;<b>x</b>" & Child & "</div>"
                'strFare = strFare & "<td><b>x</b>" & Child & "</td>"
                strFare = strFare & "</div>"
                strFare = strFare & "<div class='clear'></div>"

            End If

            If Infant > 0 Then
                Try
                    yq = 0
                    tx = 0
                    'tax = fareHashtbl("InfTax").ToString.Split("#")
                    tax = IntAirDt.Rows(0)("Inf_Tax").ToString.Split("#")
                    For i As Integer = 0 To tax.Length - 2
                        If InStr(tax(i), "YQ") Then
                            tax1 = tax(i).Split(":")
                            yq = yq + Convert.ToInt32(Math.Round(Convert.ToDecimal(tax1(1).ToString()), 0))
                        Else
                            tax1 = tax(i).Split(":")
                            tx = tx + Convert.ToInt32(Math.Round(Convert.ToDecimal(tax1(1).ToString()), 0))
                        End If
                    Next
                Catch ex As Exception
                    clsErrorLog.LogInfo(ex)
                End Try


                strFare = strFare & "<div class='large-9 medium-9 small-12 columns'>"
                strFare = strFare & "<div class='large-2 medium-2 small-2 columns'>INFANT</div>"
                strFare = strFare & "<div class='large-2 medium-2 small-2 columns'>" & IntAirDt.Rows(0)("InfBFare") & "</div>"
                strFare = strFare & "<div class='large-2 medium-3 small-3 columns'>" & yq & "</div>"
                strFare = strFare & "<div class='large-2 medium-2 small-2 columns'>" & tx & "</div>"
                strFare = strFare & "<div class='large-2 medium-2 small-2 columns'>" & IntAirDt.Rows(0)("InfFare") & "&nbsp;<b>x</b>" & Inf & "</div>"
                'strFare = strFare & "<td><b>x</b>" & Inf & "</td>"
                strFare = strFare & "</div>"
                strFare = strFare & "<div class='clear'></div>"

            End If

            strFare = strFare & "</div>"

            strFare = strFare & "<div class='row'>"
            strFare = strFare & "<div class='large-3 medium-3 small-12 rgt'>"
            strFare = strFare & "<div class='large-12 medium-12 small-12 bld blue'>Other Details</div>"
            strFare = strFare & "<div class='large-12 medium-12 small-12'>"
            strFare = strFare & "<div class='large-6 medium-6 small-6 columns'>Srv.Tax</div>"
            strFare = strFare & "<div class='large-6 medium-6 small-6 columns'>" & IntAirDt.Rows(0)("SrvTax") & "</div>"
            If (IntAirDt.Rows(0)("IsCorp") = True) Then
                strFare = strFare & "<div class='large-6 medium-6 small-6 columns'>Mgnt. Fee</div>"
                strFare = strFare & "<div class='large-6 medium-6 small-6 columns'>" & IntAirDt.Rows(0)("TOTMGTFEE") & "</div>"
            Else
                strFare = strFare & "<div class='large-6 medium-6 small-6 columns'>Tran. Fee</div>"
                strFare = strFare & "<div class='large-6 medium-6 small-6 columns'>" & IntAirDt.Rows(0)("TFee") & "</div>"
                strFare = strFare & "<div class='large-6 medium-6 small-6 columns'>Tran. Chg</div>"
                strFare = strFare & "<div class='large-6 medium-6 small-6 columns'>" & IntAirDt.Rows(0)("TC") & "</div>"
            End If

            If (VC = "SG" Or VC = "6E") Then
                strFare = strFare & "<div class='large-6 medium-6 small-6 columns'>Meal Chg.</div><div class='large-6 medium-6 small-6 columns'>" & MealPr & "</div>"
                strFare = strFare & "<div class='large-6 medium-6 small-6 columns'>Bagg Chg.</div><div class='large-6 medium-6 small-6 columns'>" & BgPr & "</div>"
            End If

            strFare = strFare & "<div class='large-6 medium-6 small-6 columns' id='trtotfare' onmouseover=funcnetfare('block','trnetfare" & FT & "'); onmouseout=funcnetfare('none','trnetfare" & FT & "'); style='cursor:pointer;color: #004b91;font-weight:bold'>Total Fare</div><div class='large-6 medium-6 small-6 columns bld'>" & Math.Round(Convert.ToDecimal(IntAirDt.Rows(0)("totFare")), 0) & "</div>"
            strFare = strFare & "<div class='large-6 medium-6 small-6 columns' id='trnetfare" & FT & "' style='display:none'>Net Fare</div><div class='large-6 medium-6 small-6 columns'>" & Math.Round(Convert.ToDecimal(IntAirDt.Rows(0)("netFare")), 0) & "</div>"
            strFare = strFare & "</div>"

            strFare = strFare & "</div>"

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
        Return strFare
    End Function

    Private Function showPaxDetails(ByVal OrderId As String, ByVal FT As String) As String
        Dim my_table As String = ""
        Try
            'dtpnr = ObjIntDetails.SelectHeaderDetail(OrderId) ' PRimary Pax
            'For i As Integer = 0 To dtpnr.Rows.Count - 1
            'Next

            dtpax = ObjIntDetails.SelectPaxDetail(OrderId, TransTD) ' Changes in SelectPaxDetail Added DOB
            my_table += "<div class='clear1'></div>"
            my_table += "<div class='row'>"

            my_table = "<div class='large-12 medium-12 small-12 bld'>Traveller Information</div>"
            my_table += "<div class='clear'></div>"
            my_table += "<div class='large-12 medium-12 small-12'>"

            my_table += "<div class='large-4 medium-4 small-4 columns bld'>Passenger Name</div>"
            my_table += "<div class='large-4 medium-4 small-4 columns bld'>Type</div>"
            my_table += "<div class='large-4 medium-4 small-4 columns bld'>DOB</div>"
            my_table += "</div>"
            For Each dr As DataRow In dtpax.Rows
                my_table += "<div class='large-12 medium-12 small-12'>"
                my_table += "<div class='large-4 medium-4 small-4 columns'>" & dr("Name").ToString() & "</div>"
                my_table += "<div class='large-4 medium-4 small-4 columns'>" & dr("PaxType").ToString() & "</div>"
                my_table += "<div class='large-4 medium-4 small-4 columns'>" & dr("DOB").ToString() & "</div>"
                my_table += "</tr>"
            Next
            my_table += "</div>"
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try


        Return my_table
    End Function

    Private Function Meal_BagDetails(ByVal OrderId As String, ByVal TransTD As String, ByVal VC As String, ByVal TT As String, ByRef FinalTotal As Double, ByVal HD As String) As String
        Dim my_table As String = ""
        Dim dtfare1 As DataSet = objSql.Get_PAX_MB_Details(OrderId, TransTD, VC, TT)
        Dim DtPxMB As DataTable = dtfare1.Tables(0)

        If DtPxMB.Rows.Count > 0 Then
            my_table += "<div class='row'>"

            my_table += "<div class='large-12 medium-12 small-12 bld'>Meals Bagagae Fare Information" + HD + "</div>"

            my_table += "<div class='large-12 medium-12 small-12'>"
            my_table += "<div class='large-1 medium-3 small-3 columns bld'>Passenger Name</div>"
            my_table += "<div class='large-1 medium-3 small-3 columns bld'>Type</div>"
            my_table += "<div class='large-1 medium-3 small-3 columns bld'>MEAL_DETAIL</div>"
            my_table += "<div class='large-1 medium-3 small-3 columns bld'>MEAL_PRICE</div>"
            my_table += "<div class='large-1 medium-3 small-3 columns bld'>BAG_DETAIL</div>"
            my_table += "<div class='large-1 medium-3 small-3 columns bld'>BAG_PRICE</div>"
            my_table += "<div class='large-1 medium-3 small-3 columns bld'>TOTAL</div>"
            my_table += "<div class='large-1 medium-3 small-3 columns bld'>Ticket No</div>"
            my_table += "</div>"
            'If TransTD = "" OrElse TransTD Is Nothing Then
            For Each dr As DataRow In DtPxMB.Rows
                my_table += "<div class='large-12 medium-12 small-12'>"
                my_table += "<div class='large-1 medium-3 small-3 columns'>" & dr("Name").ToString() & "</div>"
                my_table += "<div class='large-1 medium-3 small-3 columns'>" & dr("PaxType").ToString() & "</div>"
                my_table += "<div class='large-1 medium-3 small-3 columns'>" & dr("MDisc").ToString() & "</div>"
                my_table += "<div class='large-1 medium-3 small-3 columns'>" & dr("MPRICE").ToString() & "</div>"
                my_table += "<div class='large-1 medium-3 small-3 columns'>" & dr("BDisc").ToString() & "</div>"
                my_table += "<div class='large-1 medium-3 small-3 columns'>" & dr("BPRICE").ToString() & "</div>"
                my_table += "<div class='large-1 medium-3 small-3 columns'>" & Convert.ToDouble(Convert.ToDouble(dr("MPRICE").ToString()) + Convert.ToDouble(dr("BPRICE").ToString())) & "</div>"
                my_table += "<div class='large-1 medium-3 small-3 columns'>" & dr("TicketNumber").ToString() & "</div>"
                my_table += "</div>"
                FinalTotal += Convert.ToDouble(Convert.ToDouble(dr("MPRICE").ToString()) + Convert.ToDouble(dr("BPRICE").ToString()))
            Next
        End If
        my_table += "<div class='large-12 medium-12 small-12'>"

        my_table += "<div class='large-1 medium-3 small-3 large-push-10 medium-push-6 small-push-6 columns bld'>GRAND TOTAL </div>"
        my_table += "<div class='large-1 medium-3 small-3 columns bld' ' id='td_grandtot'    >" & FinalTotal & "</div>"
        my_table += "<div class='large-1 medium-3 small-3 columns bld'></div>"
        my_table += "</div>"
        my_table += "</div>"
        Return my_table

    End Function

    Protected Sub Submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Submit.Click

        Try
            Dim AgencyDs As DataSet
            Dim FltDs As DataSet
            Dim totFare As Double = 0
            Dim netFare As Double = 0
            FltDs = objDA.GetFltDtls(ViewState("trackid"), Session("UID"))
            AgencyDs = objDA.GetAgencyDetails(Session("UID"))
            If AgencyDs.Tables.Count > 0 And FltDs.Tables.Count > 0 Then
                If AgencyDs.Tables(0).Rows.Count > 0 And FltDs.Tables(0).Rows.Count > 0 Then
                    totFare = Convert.ToDouble(FltDs.Tables(0).Rows(0)("totFare")) '+ Convert.ToDouble(lbl_OB_TOT.Value)
                    netFare = Convert.ToDouble(FltDs.Tables(0).Rows(0)("netFare")) '+ Convert.ToDouble(lbl_OB_TOT.Value)
                    FltDs.Tables(0).Rows(0)("totFare") = totFare 'Convert.ToDouble(OBFltDs.Tables(0).Rows(0)("totFare")) + Convert.ToDouble(lbl_OB_TOT.Value)
                    FltDs.Tables(0).Rows(0)("netFare") = netFare 'Convert.ToDouble(OBFltDs.Tables(0).Rows(0)("netFare")) + Convert.ToDouble(lbl_OB_TOT.Value)
                    ' FltDs.AcceptChanges()
                    If AgencyDs.Tables(0).Rows(0)("Agent_Status").ToString.Trim <> "NOT ACTIVE" And AgencyDs.Tables(0).Rows(0)("Online_tkt").ToString.Trim <> "NOT ACTIVE" Then
                        Dim agentBal As String = ""
                        agentBal = objUMSvc.GetAgencyBal()
                        '' ''If Convert.ToDouble(AgencyDs.Tables(0).Rows(0)("Crd_Limit").ToString.Trim) > netFare Then
                        If Convert.ToDouble(agentBal) > netFare Then
                            ''Dim um As String = ""
                            ''um = objUMSvc.GetMUForPage("wait.aspx")
                            ''Response.Redirect(um & "?tid=" & ViewState("trackid") & "", False)
                            Response.Redirect("../wait.aspx?tid=" & ViewState("trackid") & "", False)
                        Else
                            ''Dim um As String = ""
                            ''um = objUMSvc.GetMUForPage("FlightInt/BookingMsg.aspx")
                            ''Response.Redirect(um & "?msg=CL", False)
                            Response.Redirect("../FlightInt/BookingMsg.aspx?msg=CL", False)
                        End If
                    Else
                        ''Dim um As String = ""
                        ''um = objUMSvc.GetMUForPage("FlightInt/BookingMsg.aspx")
                        ''Response.Redirect(um & "?msg=NA", False)
                        Response.Redirect("../FlightInt/BookingMsg.aspx?msg=NA", False)
                    End If
                Else
                    ''Dim um As String = ""
                    ''um = objUMSvc.GetMUForPage("FlightInt/BookingMsg.aspx")
                    ''Response.Redirect(um & "?msg=2", False)
                    Response.Redirect("../FlightInt/BookingMsg.aspx?msg=2", False)
                End If
            Else
                ''Dim um As String = ""
                ''um = objUMSvc.GetMUForPage("FlightInt/BookingMsg.aspx")
                ''Response.Redirect(um & "?msg=2", False)
                Response.Redirect("../FlightInt/BookingMsg.aspx?msg=2", False)
            End If
        Catch ex As Exception
            ''Dim um As String = ""
            ''um = objUMSvc.GetMUForPage("FlightInt/BookingMsg.aspx")
            ''Response.Redirect(um & "?msg=2", False)
            Response.Redirect("../FlightInt/BookingMsg.aspx?msg=2", False)
        End Try

    End Sub
End Class
