USE [TREV]
GO
/****** Object:  StoredProcedure [dbo].[Sp_insertrailcharges]    Script Date: 05/04/2019 17:41:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[Sp_insertrailcharges]
(
@Type varchar(50),
@Class varchar(50),
@agent_charge money,
@Pg_Charge money,
@updatedby varchar(50),
@massage varchar(50) out
)
as begin
if not exists(select * from IRCTC_AGENT_CHARGE where TYPE=@Type)
begin
Insert into IRCTC_AGENT_CHARGE(Type,Class,Agent_Charge,PG_Charge,Updatedate,UpdatedBy)
values(@Type,@Class,@agent_charge,@Pg_Charge,GETDATE(),@updatedby)
set @massage='Y'
end
else
begin 
set @massage='N'
end
end










---------------------------------------------------------------------------------------------------------------




USE [TREV]
GO
/****** Object:  StoredProcedure [dbo].[sp_insertledgerRailpnr]    Script Date: 05/04/2019 17:01:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[sp_insertledgerRailpnr]

(

@AGENTID VARCHAR(30)='', @TOTALAFETRDIS FLOAT='', @TRACKID VARCHAR(30)='',@RailPNR NVARCHAR(100)='',@AGENCYNAME VARCHAR(50)='', @IP VARCHAR(50), @ProjectId NVARCHAR(50)=null,

@BookedBy NVARCHAR(100)=null, @BillNo NVARCHAR(100)=null, @AVAILBAL FLOAT=NULL,@EasyID NVARCHAR(100)=null,@Status VARCHAR(50)='TICKETED',

@ERROR Varchar(100) out

)

as begin

 

Declare @AgentCrdLimit money,@DueAmount money

declare @DistrId varchar(50)='FWU';

Declare @TOTAFTERDIS_LEDGER FLOAT 

  

 

 select * from Tbl_RailPnrDetail where ledgerentry=0

 

             DECLARE @totalRecords INT

              DECLARE @I INT

              SELECT @I = 1

              SELECT @totalRecords = COUNT(*) FROM Tbl_RailPnrDetail

              if (@totalRecords>0)

              begin

              WHILE (@I <= @totalRecords)

              BEGIN

 

              declare @iid varchar(50)=''

             

             

select @iid=USER_ID,@TOTAFTERDIS_LEDGER=BOOKING_AMOUNT,@TRACKID=LTRIM(STR(TRANSACTION_ID, 25, 0)),@RailPNR=LTRIM(STR(PNR_NO, 25, 0)) from Tbl_RailPnrDetail where slno=@I
declare @type  varchar(50)
select @type=agent_type, @AGENTID=user_id,@AGENCYNAME=Agency_Name from agent_register where IrctcId=@iid    

if(@AGENTID!='')

begin
declare @agcharge money=0
declare @pgCharge money=0
select @agcharge=isnull(Agent_Charge,0) ,@pgCharge=isnull(PG_Charge,0)  from IRCTC_AGENT_CHARGE where [type]=@type
 if (@pgCharge<2)
				set @pgCharge=@TOTAFTERDIS_LEDGER*@pgCharge/100
			
              --create table #tempa( val float )
              --insert into  #tempa(val)            

              
             
             	
declare @totalcharge money=@TOTAFTERDIS_LEDGER+isnull(@agcharge,0)+isnull(@pgCharge,0)    
                           exec UpdateCrdLimit @AgentID,@totalcharge

            declare @Current_Avail_Bal Float   

    --          set @Current_Avail_Bal=( select top 1 val  from #tempa)  

                  

    --set @Current_Avail_Bal=@Current_Avail_Bal

    --          drop table #tempa

select @Current_Avail_Bal=Crd_Limit  from agent_register where IrctcId=@iid                     

                           insert into LedgerDetails(AgentID, AgencyName, InvoiceNo, PnrNo, TicketNo, TicketingCarrier, YatraAccountID,AccountID,   

                           ExecutiveID,IPAddress, Debit, Credit, Aval_Balance,BookingType, Remark,PaxId ,ProjectID,BookedBy,BillNoCorp,DueAmount,CreditLimit,DISTRID)   

                           VALUES(@AGENTID,@AGENCYNAME,@TRACKID ,@RailPNR,'','','','','',@IP,@totalcharge,0,@Current_Avail_Bal,'RailTKT','','',@ProjectId ,@BookedBy ,@BillNo ,@DueAmount, @AgentCrdLimit,@DistrId)   

                           SELECT @I = @I + 1

END

              END

              set @ERROR='YES'

              end

              else

              begin

              set @ERROR='NO'

             

              end

truncate table Tbl_RailPnrDetail

 

end

-----------------------------------------------------------------------------------------------------------




USE [TREV]
GO

/****** Object:  Table [dbo].[IRCTC_AGENT_CHARGE]    Script Date: 05/04/2019 18:11:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[IRCTC_AGENT_CHARGE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NULL,
	[Class] [varchar](50) NULL,
	[Agent_Charge] [money] NULL,
	[PG_Charge] [money] NULL,
	[Createdate] [datetime2](7) NULL,
	[Updatedate] [datetime2](7) NULL,
	[UpdatedBy] [varchar](50) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[IRCTC_AGENT_CHARGE] ADD  CONSTRAINT [DF_IRCTC_AGENT_CHARGE_Createdate]  DEFAULT (getdate()) FOR [Createdate]
GO


