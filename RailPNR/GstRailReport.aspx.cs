﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RailPNR_GstRailReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Label label = Master.FindControl("lblBC") as Label;
        label.Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Rail Import</a><a class='current' href='#'>Gst Rail Booking Report</a>";

        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
    }

    protected void btn_search_Click(object sender, EventArgs e)
    {
        try
        {
            string fromDate = Request["From"];
            if (!string.IsNullOrEmpty(fromDate))
            {
                fromDate = String.Format(fromDate.Split('-')[2], 4) + "/" + String.Format(fromDate.Split('-')[1], 2) + "/" + String.Format(fromDate.Split('-')[0], 2) + " 12:00:00 AM";
            }

            string toDate = Request["To"];
            if (!string.IsNullOrEmpty(toDate))
            {
                toDate = String.Format(toDate.Split('-')[2], 4) + "/" + String.Format(toDate.Split('-')[1], 2) + "/" + String.Format(toDate.Split('-')[0], 2) + " 23:59:59 PM";
            }

            string agenyId = Request["hidtxtAgencyName"];

            SqlTransactionDom STDom = new SqlTransactionDom();
            DataTable dtSearch = STDom.GetRailGstReport(fromDate, toDate, agenyId, ddlType.SelectedValue.ToString());

            if (dtSearch != null && dtSearch.Rows.Count > 0)
            {
                decimal sum = 0; decimal sum1 = 0; decimal sum2 = 0; decimal sum3 = 0; decimal sum5 = 0;

                for (int i = 0; i < dtSearch.Rows.Count; i++)
                {
                    sum += Convert.ToDecimal(dtSearch.Rows[i]["Amount"].ToString());
                    sum1 += Convert.ToDecimal(dtSearch.Rows[i]["pg"].ToString());
                    sum2 += Convert.ToDecimal(dtSearch.Rows[i]["agcharge"].ToString());
                    sum3 += Convert.ToDecimal(dtSearch.Rows[i]["Totalfare"].ToString());
                    sum5 += Convert.ToDecimal(dtSearch.Rows[i]["gstcharge"].ToString());
                }

                lblpnr.Text = dtSearch.Rows.Count.ToString();
                lbltktare.Text = sum.ToString();
                lblpg.Text = sum1.ToString();
                lblAg.Text = sum2.ToString();
                lblgst.Text = sum5.ToString();
                lbltotamount.Text = sum3.ToString();
            }

            Grid_Ledger.DataSource = dtSearch;
            Grid_Ledger.DataBind();
        }
        catch (Exception ex)
        {
            ex.ToString();
            throw;
        }
    }
}