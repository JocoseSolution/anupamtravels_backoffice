﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="GstRailReport.aspx.cs" Inherits="RailPNR_GstRailReport" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script>
        function CheckGstReport() {
            let fromdate = $("#From").val(); let todate = $("#To").val();
            if (fromdate.trim() == "") { alert("Please enter from date."); $("#From").focus(); return false; }
            if (todate.trim() == "") { alert("Please enter from date."); $("#To").focus(); return false; }
        }
    </script>
    <div class="row">
        <div class="container-fluid" style="padding-right: 35px;">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-2">
                                <%--<label style="color:#ccc;font-weight:bold;">Ledger From Date</label>--%>
                                <div class="input-group">                                    
                                    <input type="text" placeholder="From Date" name="From" id="From" readonly="readonly" class="form-control input-text full-width" />
                                    <span class="input-group-addon" style="background: #49cced">
                                        <span class="glyphicon glyphicon-calendar cd" style="cursor: pointer;"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <%--<label style="color:#ccc;font-weight:bold;">Ledger To Date</label>--%>
                                <div class="input-group">
                                    <input type="text" placeholder="To Date" name="To" id="To" readonly="readonly" class="form-control input-text full-width" />
                                    <span class="input-group-addon" style="background: #49cced">
                                        <span class="glyphicon glyphicon-calendar cd1" style="cursor: pointer;"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <%--<label style="color:#ccc;font-weight:bold;">Agency Name</label>--%>
                                <div class="input-group" id="tr_Agency" runat="server">
                                    <span id="tr_AgencyName" runat="server">
                                        <input type="text" placeholder="Agency Name" id="txtAgencyName" name="txtAgencyName" autocomplete="off" class="form-control input-text full-width" />
                                        <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" /></span>
                                    <span class="input-group-addon" style="background: #49cced">
                                        <span class="fa fa-black-tie"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <%--<label style="color:#ccc;font-weight:bold;">Action Type</label>--%>
                                <div class="input-group">
                                    <asp:DropDownList ID="ddlType" runat="server">
                                        <asp:ListItem Value="debit">Success</asp:ListItem>
                                        <asp:ListItem Value="credit">Refunded</asp:ListItem>                                        
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <%--<label style="color:#ccc;font-weight:bold;">&nbsp;</label>--%>
                                <div class="input-group">
                                    <asp:Button ID="btn_search" runat="server" Text="Get Result" CssClass="btn btn-success" Style="width: 200px; height: 33px;" OnClick="btn_search_Click" OnClientClick="return CheckGstReport();" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <%--<label style="color:#ccc;font-weight:bold;">&nbsp;</label>--%>
                                <div class="input-group">
                                    <asp:Button ID="btn_export" runat="server" Text="Export To Excel" CssClass="btn btn-success" Style="width: 200px; height: 33px;" />
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top:28px" >
                              <div class="col-md-2">
                                <div class="input-group">
                                    Total PNR:
                                    <asp:Label ID="lblpnr" runat="server"></asp:Label>
                                    </div>
                                </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    Total Ticket Fare:
                                    <asp:Label ID="lbltktare" runat="server"></asp:Label>
                                    </div>
                                </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    Total Pg:
                                    <asp:Label ID="lblpg" runat="server"></asp:Label>
                                    </div>
                                </div>

                            <div class="col-md-2">
                                <div class="input-group">
                                    Total Agent Charge:
                                    <asp:Label ID="lblAg" runat="server"></asp:Label>
                                    </div>
                                </div>
                                                            <div class="col-md-2">
                                <div class="input-group">
                                    Total GST Charge:
                                    <asp:Label ID="lblgst" runat="server"></asp:Label>
                                    </div>
                                </div>

                            <div class="col-md-2">
                                <div class="input-group">
                                    Total Amount:
                                    <asp:Label ID="lbltotamount" runat="server"></asp:Label>
                                    </div>
                                </div>

                            </div>
                        <br />
                        <div id="Div1" class="row" style="background-color: #fff; overflow-y: scroll;" runat="server" visible="true">
                            <div class="col-md-12">

                                <asp:UpdatePanel ID="up" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="Grid_Ledger" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-striped table-bordered table-hover" Style="text-transform: uppercase; color: #000;"
                                            GridLines="None" PageSize="30">
                                            <Columns>
                                                <asp:BoundField DataField="Agencyname" HeaderText="Agentid" />
                                                <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                                <asp:BoundField DataField="pg" HeaderText="Pg charges" />
                                                <asp:BoundField DataField="agcharge" HeaderText="Agent charges" />
                                                <asp:BoundField DataField="Totalfare" HeaderText="Total Fare" />
                                                <asp:BoundField DataField="gstcharge" HeaderText="GST Charge" />
                                                <asp:BoundField DataField="ReservationId" HeaderText="PNR No." />
                                                <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" />
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>

