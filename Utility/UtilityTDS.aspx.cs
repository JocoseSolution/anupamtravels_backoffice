﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Utility_UtilityTDS : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            Response.Redirect("~/Login.aspx");
        }
        else
        {
            Label lblBc = (Label)Master.FindControl("lblBC");
            lblBc.Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Utility </a><a class='current' href='#'>TDS</a></a>"; ;
            if (!IsPostBack)
            {
                BindGridview();
            }
        }
    }

    protected void BindGridview()
    {
        try
        {
            SqlDataAdapter sda = new SqlDataAdapter("select * from  T_SMBPUtility_TDS", con);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            utilitytds_grid.DataSource = dt;
            utilitytds_grid.DataBind();
        }
        catch (Exception ex)
        {
            ex.ToString();
            throw;
        }
    }

    protected void utilitytds_grid_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            utilitytds_grid.EditIndex = -1;
            BindGridview();

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void utilitytds_grid_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            utilitytds_grid.EditIndex = e.NewEditIndex;
            BindGridview();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void utilitytds_grid_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            Label lblSNo = (Label)(utilitytds_grid.Rows[e.RowIndex].FindControl("lblId"));
            int Id = Convert.ToInt32(lblSNo.Text.Trim().ToString());
            DropDownList txtType = (DropDownList)utilitytds_grid.Rows[e.RowIndex].FindControl("ddlType");
            TextBox txtTds = (TextBox)utilitytds_grid.Rows[e.RowIndex].FindControl("txtTDS");

            SqlCommand cmd = new SqlCommand("sp_UpdateSMBPUtilityTDS", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("Id", Id);
            cmd.Parameters.AddWithValue("Type", txtType.SelectedValue);
            cmd.Parameters.AddWithValue("TDS", txtTds.Text.Trim());

            con.Open();
            int k = cmd.ExecuteNonQuery();

            utilitytds_grid.EditIndex = -1;
            BindGridview();

            if (k > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully updated.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('tryagain.');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('" + ex.Message + "');", true);
        }
    }

    protected void utilitytds_grid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string item = e.Row.Cells[0].Text;
            foreach (Button button in e.Row.Cells[2].Controls.OfType<Button>())
            {
                if (button.CommandName == "Delete")
                {
                    button.Attributes["onclick"] = "if(!confirm('Do you want to delete?')){ return false; };";
                }
            }
        }
    }

    protected void utilitytds_grid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        utilitytds_grid.PageIndex = e.NewPageIndex;
        this.BindGridview();
    }
}