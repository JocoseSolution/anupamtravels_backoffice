﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class Utility_UtilityReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Try
            CType((Page.Master.FindControl("lblBC")), Label).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Reports </a><a class='current' href='#'>Utility Details</a></a>"
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If

            If Not IsPostBack Then
                CheckEmptyValue()
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Protected Sub btn_result_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_result.Click
        CheckEmptyValue()
    End Sub

    Public Sub CheckEmptyValue()
        Dim FromDate As String
        Dim ToDate As String
        If [String].IsNullOrEmpty(Request("From")) Then
            
            FromDate = ""
           
        Else
            FromDate = Strings.Right((Request("From")).Split(" ")(0), 4) + "-" + Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "-" + Strings.Left((Request("From")).Split(" ")(0), 2)  'Date Format 2017-04-09 12:00:00 AM'
            FromDate = FromDate + " " + "12:00:00 AM"
        End If
        If [String].IsNullOrEmpty(Request("To")) Then
            ToDate = ""
        Else
            ToDate = Right((Request("To")).Split(" ")(0), 4) & "-" & Mid((Request("To")).Split(" ")(0), 4, 2) & "-" & Left((Request("To")).Split(" ")(0), 2)
            ToDate = ToDate & " " & "11:59:59 PM"
        End If

        Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "Agency Name or ID", "", Request("hidtxtAgencyName"))
        Dim ClientRefId As String = txt_ClientRefId.Text
        Dim TransType As String = ddltransType.SelectedValue
        Dim Status As String = ddlstatus.SelectedValue

        Dim dtRecord As New DataSet
        dtRecord = GetRechargeTrancReportList(FromDate, ToDate, AgentID, ClientRefId, TransType, Status)
        Session("dtRecord") = dtRecord
        ticket_grdview.DataSource = dtRecord
        ticket_grdview.DataBind()
    End Sub

    Public Function GetRechargeTrancReportList(ByVal FromDate As String, ByVal ToDate As String, ByVal AgentID As String, ByVal ClientRefId As String, ByVal TransType As String, ByVal Status As String) As DataSet
        Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        Dim ds1 As New DataSet
        Dim cmd As New SqlCommand()
        Dim da As New SqlDataAdapter(cmd)
        Dim adap As New SqlDataAdapter()

        cmd.CommandText = "sp_GetRchargeTransDetails"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@AgentId", SqlDbType.VarChar).Value = If([String].IsNullOrEmpty(AgentID), Nothing, AgentID)
        cmd.Parameters.Add("@ClientRefId", SqlDbType.VarChar).Value = If([String].IsNullOrEmpty(ClientRefId), Nothing, ClientRefId)
        cmd.Parameters.Add("@TransType", SqlDbType.VarChar).Value = If([String].IsNullOrEmpty(TransType), Nothing, TransType)
        cmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = If([String].IsNullOrEmpty(FromDate), Nothing, FromDate)
        cmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = If([String].IsNullOrEmpty(ToDate), Nothing, ToDate)
        cmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = If([String].IsNullOrEmpty(Status), Nothing, Status)
        cmd.Connection = con1
        da.Fill(ds1)
        Return ds1
    End Function
End Class
