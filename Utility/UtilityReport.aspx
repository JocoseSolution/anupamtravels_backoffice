﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false" CodeFile="UtilityReport.aspx.vb" Inherits="Utility_UtilityReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="<%=ResolveUrl("~/Hotel/css/B2Bhotelengine.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#From").datepicker({ dateFormat: 'dd-mm-yy' }).val();
            $(".cd").click(function () {
                $("#From").focus();
            });
            $("#To").datepicker({ dateFormat: 'dd-mm-yy' }).val();
            $(".cd1").click(function () {
                $("#To").focus();
            });
        });
    </script>
    <style type="text/css">
	th {
            text-align: center;
        }

        span {
            color: #000;
            text-align: center;
        }
        .panel-body {
            height: 100%;
            overflow: inherit;
        }

        #slidediv {
            /*color: #F25022;
            background-color: #fff;
            border: 2px solid #00A4EF;*/
            display: none;
            /*height: 550px;*/
        }

            #slidediv p {
                margin: 15px;
                font-size: 0.917em;
            }

        #contentdiv {
            clear: both;
            margin: 0 auto;
            max-width: initial;
        }
    </style>

    <style>
        .overlay {
            position: fixed;
            /*top: 102px;
            left: 287px;*/
            right: -12px;
            bottom: 0;
            width: auto;
            height: 70%;
            /*background: #fff;
            opacity: 1.5;
            z-index: 1;
            display: inline-block;*/
        }

        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">
                <div class="panel panel-primary">

                    <div class="panel-body" style="margin-right: -146px">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="input-group" id="td_Agency" runat="server">
                                    <input type="text" id="txtAgencyName" placeholder="Agency Name" name="txtAgencyName" class="form-control input-text full-width" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" />
                                    <input type="hidden" id="hidtxtAgencyName" placeholder="Agency Name" class="form-control input-text full-width" name="hidtxtAgencyName" value="" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-briefcase"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <input type="text" placeholder="From Date" name="From" id="From" class="form-control input-text full-width" readonly="readonly" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-calendar cd" style="cursor: pointer"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="input-group">
                                    <input type="text" placeholder="To Date" name="To" id="To" class="form-control input-text full-width" readonly="readonly" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-calendar cd1" style="cursor: pointer"></span>
                                    </span>

                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <asp:TextBox ID="txt_ClientRefId" placeholder="Client Ref. Id" runat="server" class="form-control input-text full-width"></asp:TextBox>
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-list-alt"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:DropDownList class="input-text full-width" ID="ddltransType" runat="server">
                                        <asp:ListItem Value="">All</asp:ListItem>
                                        <asp:ListItem Value="mobile">Mobile</asp:ListItem>
                                        <asp:ListItem Value="dth">DTH</asp:ListItem>
                                        <asp:ListItem Value="electricity">Electricity</asp:ListItem>
                                        <asp:ListItem Value="landline">Landline</asp:ListItem>
                                        <asp:ListItem Value="insurance">Insurance</asp:ListItem>
                                        <asp:ListItem Value="gas">Gas</asp:ListItem>
                                        <asp:ListItem Value="broadband">Broadband</asp:ListItem>
                                        <asp:ListItem Value="water">Water</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2" style="top: 8px">
                                <div class="form-group">
                                    <asp:DropDownList class="input-text full-width" ID="ddlstatus" runat="server">
                                        <asp:ListItem Value="">All</asp:ListItem>
                                        <asp:ListItem Value="successful">Success</asp:ListItem>
                                        <asp:ListItem Value="under process">Under Process</asp:ListItem>
                                        <asp:ListItem Value="failed">Failed</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2" style="top: 8px">
                                <div class="form-group">
                                    <asp:Button ID="btn_result" runat="server" Text="Find" Width="74px" CssClass="btn btn-success" />
                                     <!--<asp:Button ID="btn_export" runat="server" CssClass="btn btn-success" Text="Export To Excel" />-->
                                </div>
                            </div>
                           <!-- <div class="col-md-2" style="top: 8px">
                                <div class="form-group">
                                    <span class="badge cyan" style="background-color: #49cced; box-shadow: 3px 3px #2dadce;">
                                        <label for="exampleInputPassword1" style="color: white">
                                            Total Records :
                                        <asp:Label ID="lbl_counttkt" runat="server">65</asp:Label>
                                        </label>
                                    </span>
                                </div>
                            </div>-->
                        </div>
                        <div class="row">
                            <div class="row">
                                <div class="col-md-11">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                                        <ContentTemplate>
                                            <asp:GridView ID="ticket_grdview" runat="server" AllowPaging="True" AllowSorting="True"
                                                AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover"
                                                GridLines="None" PageSize="30" Style="text-transform: uppercase;">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="TxnDate">
                                                        <ItemTemplate>
                                                            <asp:Label ID="TxnDate" runat="server" Text='<%#Eval("transDate")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="AgentID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="AgentID" runat="server" Text='<%#Eval("AgentId")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="OrderID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="OrderID" runat="server" Text='<%#Eval("TransactionId")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Client Ref. ID">
                                                        <ItemTemplate>
                                                            <asp:Label ID="ClientRefID" runat="server" Text='<%#Eval("ClientRefId")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Number">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Number" runat="server" Text='<%#Eval("Number")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Amount">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Amount" runat="server" Text='<%#Eval("Amount")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ServiceType">
                                                        <ItemTemplate>
                                                            <asp:Label ID="ServiceType" runat="server" Text='<%#Eval("ServiceType")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Operator">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Operator" runat="server" Text='<%#Eval("Operator")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="CircleName">
                                                        <ItemTemplate>
                                                            <asp:Label ID="CircleName" runat="server" Text='<%#Eval("CircleName ")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                                <RowStyle CssClass="RowStyle" />
                                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                <PagerStyle CssClass="PagerStyle" />
                                                <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                <HeaderStyle CssClass="HeaderStyle" />
                                                <EditRowStyle CssClass="EditRowStyle" />
                                                <AlternatingRowStyle CssClass="AltRowStyle" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>

