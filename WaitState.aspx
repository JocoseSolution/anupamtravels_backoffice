<%@ Page Language="VB" AutoEventWireup="false" CodeFile="WaitState.aspx.vb" Inherits="Wait" ValidateRequest="false" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Please Wait...</title>
    <link href="css/main2.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="wait" style="text-align: center; margin-top: 50px; font-size: 16px;">
        Booking Is Under Process.Please Wait...<br />
        <br />
        <br />
        <img src='<%=ResolveUrl("~/images/wait.gif")%>' alt="" /><br />
        <br />
        <br />
        Please Don't Close This Window.
    </div>
    <%If Session("search_type").ToString = "Adv" Then%>

    <script type="text/javascript">
          window.location.href='Adv_Search/Book_con.aspx';
    </script>

    <%  ElseIf Session("search_type").ToString = "Flt" Then%>

    <script type="text/javascript">
        window.location.href = 'FlightDom/BookingConfimation.aspx?OBTID=<%=Request("OBTID")%>&IBTID=<%=Request("IBTID") %>&FT=<%=Request("FT") %>';
    </script>
    <%  ElseIf Session("search_type").ToString = "RTF" Then%>

    <script type="text/javascript">
        window.location.href = 'LccRF/LccRTFBooking.aspx?OBTID=<%=Request("OBTID")%>&IBTID=<%=Request("IBTID") %>&FT=<%=Request("FT") %>';
    </script>
  <%  ElseIf Session("search_type").ToString = "FltInt" Then%>

    <script type="text/javascript">
        window.location.href = 'FlightInt/BookingConfimation.aspx?TID=<%=Request("tid")%>';
    </script>
    <%  Else%>
    <%  End If%>
    </form>
</body>
</html>

