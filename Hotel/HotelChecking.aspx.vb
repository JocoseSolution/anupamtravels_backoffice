﻿Imports System.Net
Imports System.IO
Imports System.IO.Compression
Imports System.Text
Imports System.Linq
Imports System.Xml.Linq
Partial Class Hotel_HotelTesting
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            TextBox2.Text = GTAPostXml(TextBox3.Text.Trim(), TextBox1.Text.Trim())
        Catch ex As Exception
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
            TextBox2.Text = ex.Message
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            TextBox3.Text = "http://stage-api.travelguru.com/services-2.0/tg-services/TGServiceEndPoint"
            Dim ReqXml As StringBuilder = New StringBuilder()
            Try
                ReqXml.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>")
                ReqXml.Append("<soap:Body>")
                ReqXml.Append("<OTA_HotelAvailRQ xmlns='http://www.opentravel.org/OTA/2003/05' RequestedCurrency='INR' SortOrder='DEALS' Version='0.0' PrimaryLangID='en' SearchCacheLevel='VeryRecent'>")
                ReqXml.Append("<AvailRequestSegments>")
                ReqXml.Append("<AvailRequestSegment>")
                ReqXml.Append("<HotelSearchCriteria>")
                ReqXml.Append("<Criterion>")
                ReqXml.Append("<Address>")
                ReqXml.Append("<CityName>New Delhi</CityName>")
                ReqXml.Append("<CountryName Code='India'></CountryName>")
                ReqXml.Append("</Address>")
                ReqXml.Append("<HotelRef/>")
                ReqXml.Append("<StayDateRange End='2013-12-30' Start='2013-12-27'/>")
                ReqXml.Append("<RoomStayCandidates>")
                ReqXml.Append("<RoomStayCandidate>")
                ReqXml.Append("<GuestCounts>")
                ReqXml.Append("<GuestCount AgeQualifyingCode='10' Count='1'/>")
                ReqXml.Append("</GuestCounts>")
                ReqXml.Append("</RoomStayCandidate>")
                ReqXml.Append("</RoomStayCandidates>")
                ReqXml.Append("<TPA_Extensions>")
                ReqXml.Append("<Pagination enabled='false' />")
                ReqXml.Append("<HotelBasicInformation>")
                ReqXml.Append("<Reviews/>")
                ReqXml.Append("</HotelBasicInformation>")
                ReqXml.Append("<UserAuthentication password='test' propertyId='1300000141' username='testnet'/>")
                ReqXml.Append("<Promotion Type='BOTH' Name='ALLPromotions' />")
                ReqXml.Append("</TPA_Extensions>")
                ReqXml.Append("</Criterion>")
                ReqXml.Append("</HotelSearchCriteria>")
                ReqXml.Append("</AvailRequestSegment>")
                ReqXml.Append("</AvailRequestSegments>")
                ReqXml.Append("</OTA_HotelAvailRQ>")
                ReqXml.Append("</soap:Body>")
                ReqXml.Append("</soap:Envelope>")
                TextBox1.Text = ReqXml.ToString()
            Catch ex As Exception
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "")
                TextBox2.Text = ex.Message
            End Try
        End If
    End Sub

    Protected Function GTAPostXml(ByVal url As String, ByVal xml As String) As String
        Dim sbResult As New StringBuilder
        Try
            Dim Http As HttpWebRequest = WebRequest.Create(url)
            If Not String.IsNullOrEmpty(xml) Then
                Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate")
                Http.Method = "POST"
                Dim lbPostBuffer As Byte() = Encoding.UTF8.GetBytes(xml)
                Http.ContentLength = lbPostBuffer.Length
                Http.ContentType = "text/xml"
                'Http.Timeout = 300000
                Using PostStream As Stream = Http.GetRequestStream()
                    PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length)
                End Using
            End If

            Using WebResponse As HttpWebResponse = Http.GetResponse()
                If WebResponse.StatusCode <> HttpStatusCode.OK Then
                    Dim message As String = [String].Format("POST failed. Received HTTP {0}", WebResponse.StatusCode)
                    Throw New ApplicationException(message)
                Else
                    Dim responseStream As Stream = WebResponse.GetResponseStream()
                    If (WebResponse.ContentEncoding.ToLower().Contains("gzip")) Then
                        responseStream = New GZipStream(responseStream, CompressionMode.Decompress)
                    ElseIf (WebResponse.ContentEncoding.ToLower().Contains("deflate")) Then
                        responseStream = New DeflateStream(responseStream, CompressionMode.Decompress)
                    End If
                    Dim reader As StreamReader = New StreamReader(responseStream, Encoding.Default)
                    sbResult.Append(reader.ReadToEnd())
                    responseStream.Close()
                End If
            End Using
        Catch WebEx As WebException
            TextBox2.Text = WebEx.Message
            Dim response As WebResponse = WebEx.Response
            Dim stream As Stream = response.GetResponseStream()
            Dim responseMessage As [String] = New StreamReader(stream).ReadToEnd()
            sbResult.Append(responseMessage)
        Catch ex As Exception
            sbResult.Append(ex.Message)
            TextBox2.Text = ex.Message
        End Try
        Return sbResult.ToString()
    End Function
End Class
